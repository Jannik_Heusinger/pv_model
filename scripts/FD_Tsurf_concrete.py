# 1D Finite difference scheme to solve for the surface temperature in the desert
# latent heat fluxes are neglected for now
# Neumann conditions at the upper and lower boundaries
# at the lower boundary the heat flux is assumed to be 0

from constants import *

def FD_Tsurf_concrete(T,QG):
    #z = np.linspace(0, L, num=nz-1) # Grid
    # Thermal diffusivity on vertical grid
    kappa = [kappa_c]*nz # thermal diffusivity of concrete
    kappa_ss = [kappa_s]*(nz-8)
    kappa[0:nz-8] = kappa_ss # thermal diffusivity of sand

    # explicit model, forward in time, central in space
    c = QG/-k # heat flux condition outside
    c1 = 0./-k # heat flux condition inside
    # Compute new temperature
    Tnew = [0]*nz
    sz = kappa[nz-1]*dt/dz**2
    for i in range(1,nz-1):
        Tnew[i]=T[i]+kappa[i]*dt*((T[i+1]-2*T[i]+T[i-1])/(dz**2))

    # Set boundary conditions
    Tnew[0]= T[0]+sz*(2*T[0+1]-2*T[0]-2*dz*c1) # inside boundary condition; zero fluxes
    #Tnew[0]= 20+273.15 # inside boundary condition; zero fluxes
    Tnew[nz-1]=T[nz-1]+sz*(2*T[nz-2]-2*T[nz-1]-2*dz*c) # outside boundary condition

    # update temperature and time
    T = Tnew
    return T
