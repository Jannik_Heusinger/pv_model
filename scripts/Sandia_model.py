## Sandia model to calculate Pout
import math
from switches import PV_type
from constants import W,L


def SandiaModel(Ibc,Idc,Ws,Ta,SolZen,Tm,IncAng,AA,NcellSer, Isc0, Imp0, Vmp0, aIsc, aImp, C0, C1, BVmp, DiodeFactor, C2, C3, a0, a1, a2, a3, a4, b0, b1, b2, b3, b4, b5, DT0, fd, a, b):
### Input variables needed
# Ibc: direct radiation on collector plane (W/m2)
# Idc: diffuse radiation on collector plante (W/m2)
# Ws: wind speed (m/s)
# Ta: ambient temperature (°C)
# SolZen: solar zenith angle (°)
# Tm: module temperature (°C)
# IncAng: incidence angle (°)
# Altitude: altitude (m)

### Calculcated variables
#Tc: cell temperature (°C)
# Ee: effective irradiance (W/m2)
# F1: Sandia F1 function for air mass effects
# F2: Sandia F2 function of incidence angle
# AMa: absolute air mass

### Constants
# Imp0: current at MPP at SRC (1000 W/m2, 25 C) (A)
# aImp: Imp temperature coefficient (°C**-1)
# C0-C3: empirical module-specific constants
# a,b: empirical module-specific constants
# DT0: deltaT in Table 1 Davis et al. 2002
# fd: fraction of diffuse irradiance used by module, 1 for non-concentrating modules
# Isc0: Isc at Tc=25 C, Ic = 1000 W/m2 (A)
# aIsc: Isc temperature coefficient (°C**-1)
# Vmp0: Vmp at SRC (1000 W/m2, 25 C) (V)
# NcellSer: # cells in series
# DiodeFactor: module-specific empirical constant
# BVmp: Vmp temperature coefficient (V/C)

    # a = -3.562 #Table 1 Davis et al. 2002
    # b = -0.0786 #Table 1 Davis et al. 2002
    DT0 = 3 # temperature difference between cell and back of module at 1000 W/m2 (K)
    Altitude = 100
    # fd = 1

    # if PV_type == 'Polycrystalline':
    #     a0= 0.918093
    #     a1= 0.086257
    #     a2= -0.024459
    #     a3= 0.002816
    #     a4= -0.000126
    #     b0= 0.99851
    #     b1= 1.2122*10**(-2)
    #     b2= 1.4398*10**(-3)
    #     b3= -5.5759*10**(-5)
    #     b4= 8.7794*10**(-7)
    #     b5= -4.9190*10**(-9)
    #     C0= 1.014
    #     C1= -0.005
    #     C2= -0.321
    #     C3= -30.201
    #     Imp0= 3.818
    #     aImp= 0.000047
    #     Isc0= 4.250
    #     aIsc= 0.000560
    #     NcellSer= 72
    #     Vmp0= 32.944
    #     DiodeFactor= 1.025
    #     BVmp= -0.159116
    # elif PV_type == 'Monocrystalline':
    #     a0= 0.935823
    #     a1= 0.054289
    #     a2= -0.008677
    #     a3= 0.000527
    #     a4= -0.000011
    #     b0= 1.00034
    #     b1= -5.5575*10**(-3)
    #     b2= 6.553*10**(-4)
    #     b3= -2.7299*10**(-4)
    #     b4= 4.6405*10**(-7)
    #     b5= -2.8061*10**(-9)
    #     C0= 1.0
    #     C1= 0.003
    #     C2= -0.538
    #     C3= -21.408
    #     Imp0= 3.961
    #     aImp= -0.000390
    #     Isc0= 4.375
    #     aIsc= 0.000401
    #     NcellSer= 72
    #     Vmp0= 33.680
    #     DiodeFactor= 1.026
    #     BVmp= -0.153578

    E = Ibc + fd * Idc
    #Tm = E * math.exp(a + b * Ws) + Ta
    Tc = Tm + (E/1000.0) * DT0
    # absolute air mass
    if (SolZen < 89.9):
        AM = 1.0/math.cos(SolZen * math.pi/180) + 0.5057 * (96.08 - SolZen)**(-1.634)
        AMa = math.exp(-0.0001184 * Altitude) * AM
    else:
        AM = 36.32
        AMa = math.exp(-0.0001184 * Altitude) * AM

    # eq.8 Davis et al., 2002
    F1 = (a0 + a1 * AMa + a2 * AMa**2 + a3 * AMa**3 + a4 * AMa**4)
    if(SolZen >= 89.9 or F1 < 0.0):
        F1 = 0.0
    # eq.9 Davis et al., 2002
    F2 = b0 + b1 * IncAng + b2 * IncAng**2 + b3 * IncAng**3 + b4 * IncAng**4 + b5 * IncAng**5
    if (SolZen >= 89.9 or F2 < 0.0):
        F2 = 0.0
    # Short-circuit current (A)
    Isc = Isc0 * F1 * ((Ibc * F2 + fd * Idc)/1000.0) * (1.0 + aIsc * (Tc - 25.0))

    Ee = Isc/((1.0 + aIsc * (Tc - 25.0)) * Isc0) # effective irradiance

    # Current at maximum power point (A)
    Imp = Imp0 * (C0 * Ee + C1 * Ee**2) * (1.0 + aImp * (Tc - 25))
    # Voltage at maximum power point (V)
    if(Ee > 0.0):
        dTc = (DiodeFactor * (1.38066*10**(-23) * (Tc + 273.15))/1.60218*10**(-19))

        Vmp = Vmp0 + C2 * NcellSer * dTc * math.log(Ee) + C3 * NcellSer * (dTc * math.log(Ee))**2 + BVmp * (Tc - 25.0)
    else:
        Vmp = 0.0
    #print(Vmp)
    Pmp = Imp * Vmp # Power at maximum power point (W)
    Pmp = Pmp/AA
    return Pmp

# def SandiaIx_fun(Tc,Ee,Ix0,aIsc,aImp,C4,C5):
#     # Tc: cell temperature (°C)
#     # Ee: effective irradiance (W/m2)
#     # Ix0: Ix at SRC (1000 W/m2, 25 C) (A)
#     # aIsc: Isc temperature coefficient (°C**-1)
#     # aImp: Imp temperature coefficient (°C**-1)
#     # C4,C5: empirical module-specific constants
#
#     Ix = Ix0 * (C4 * Ee + C5 * (Ee)**2 * (1.0 + ((aIsc + aImp)/ 2.0 * (Tc - 25.0))))
#
#     return SandiaIx
#
# def SandiaIxx_fun(Tc,Ee,Ixx0,aImp,C6,C7):
#     # Tc: cell temperature (°C)
#     # Ee: effective irradiance (W/m2)
#     # Ixx0: Ixx at SRC (1000 W/m2, 25 C) (A)
#     # aImp: Imp temperature coefficient (°C**-1)
#     #C6,C7: empirical module-specific constants
#
#     SandiaIxx = Ixx0 * (C6 * Ee + C7 * Ee**2) * (1.0 + aImp * (Tc - 25.0))
#
#     return SandiaIxx
#
#
#
# if (Ee > 0.0):
#     BVocEe = BVoc0 + mBVoc * (1.0 - Ee)
#
#     SandiaVoc = Voc0 + NcellSer * dTc * math.log(Ee) + BVocEe * (Tc - 25.0)
# else:
#     SandiaVoc = 0.0
