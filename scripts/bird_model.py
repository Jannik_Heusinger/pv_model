## script to calculate DNI and diffuse after Bird 1984
import math
from constants import *
import pandas as pd
Ozone = 0.3
H2O = 1.5
Taua = 0.08
Ba = 0.85

def bird_model(P,theta_zh,global_rad,doy):
    ETR =1367*(1.00011+0.034221*math.cos(2*math.pi*(doy-1)/365)+0.00128*math.sin(2*math.pi*(doy-1)/365)+0.000719*math.cos(2*(2*math.pi*(doy-1)/365))+0.000077*math.sin(2*(2*math.pi*(doy-1)/365)))
    if theta_zh<89.:
        Air_mass = 1/(math.cos(theta_zh/(180/math.pi))+0.15/(93.885-theta_zh)**1.25)
    else:
        Air_mass = 0.

    if Air_mass > 0.:
        T_rayleigh = math.exp(-0.0903*(P*Air_mass/1013)**0.84*(1+P*Air_mass/1013-(P*Air_mass/1013)**1.01))
        T_ozone = 1-0.1611*(Ozone*Air_mass)*(1+139.48*(Ozone*Air_mass))**-0.3034-0.002715*(Ozone*Air_mass)/(1+0.044*(Ozone*Air_mass)+0.0003*(Ozone*Air_mass)**2)
        T_gases = math.exp(-0.0127*(Air_mass*P/1013)**0.26)
        T_water = 1-2.4959*Air_mass*H2O/((1+79.034*H2O*Air_mass)**0.6828+6.385*H2O*Air_mass)
        T_aerosol = math.exp(-(Taua**0.873)*(1+Taua-Taua**0.7088)*Air_mass**0.9108)
        TAA = 1-0.1*(1-Air_mass+Air_mass**1.06)*(1-T_aerosol)
        rs = 0.0685+(1-Ba)*(1-T_aerosol/TAA)
        ld = 0.9662*ETR*T_aerosol*T_water*T_gases*T_ozone*T_rayleigh
        las = ETR*math.cos(theta_zh/(180/math.pi))*0.79*T_ozone*T_gases*T_water*TAA*(0.5*(1-T_rayleigh)+Ba*(1-(T_aerosol/TAA)))/(1-Air_mass+(Air_mass)**1.02)

    else:
        T_rayleigh = 0.
        T_ozone = 0.
        T_gases = 0.
        T_water = 0.
        T_aerosol = 0.
        TAA = 0.
        rs = 0.
        ld = 0.
        las = 0.
    if theta_zh < 90.:
        ldnh = ld*math.cos(theta_zh/(180/math.pi))
    else:
        ldnh = 0.
    if Air_mass > 0.:
        GH = (ldnh+las)/(1-ground_albedo*rs)
        Kdif_model = GH- ldnh
        DNI = (global_rad*(1-ground_albedo*rs)-las)/math.cos(theta_zh/(180/math.pi))
        Direct_hor = DNI*math.cos(theta_zh/(180/math.pi))
        Kdif = global_rad - Direct_hor
    else:
        GH = 0.
        DNI = 0.
        Direct_hor = 0.
        Kdif = 0.
        Kdif_model = 0.

    return DNI,Kdif,GH,ldnh,ld,Kdif_model,T_rayleigh,T_ozone,T_gases,T_water,T_aerosol,Air_mass
