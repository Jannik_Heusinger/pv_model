# Calculates the surface and ground fractions seen by a sensor above
# a PV array for different tilt angles of the PV modules

import pandas as pd
from NR01_longwave import sensor_raytracing

# Define an output table for collecting model results
Output_table = pd.DataFrame(columns = ['beta_PV','PVF_up','PVF_down','GSF','GUF'])

beta_PV = []
for i in range(1,60):
    beta_PV.append(i)
    print(beta_PV[i-1])
    PVF_up,PV_down,GSF,GUF = sensor_raytracing(beta_PV[i-1],0.1)

    Output_table= Output_table.append({'beta_PV':beta_PV[i-1],'PVF_up':PVF_up,'PVF_down':PVF_down,'GSF':GSF,'GUF':GUF},ignore_index=True)


Output_table.to_csv('../outputs/Raytracing_surface_fractions.csv')

#PVF,GSF,GUF = sensor_raytracing(0.5,0.1)
