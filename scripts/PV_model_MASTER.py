# PV model based on Jones and Underwood (2001)

# import modules, files and all constants needed

from solar_geo import solar_geo
from lookup_dates import lookup
from diffuse_rad_Krayenhoff import CLRSKY
from ld_mod import ld_mod
from constants import *
from switches import *
from shade_frc import shade_frc_fct
from shade_frc_new import shade_frc_gen
from force_restore import Ts_calc_surf
from FD_Tsurf import FD_Tsurf
from bird_model import bird_model
from perez_ineichen import get_relative_airmass,get_absolute_airmass,ineichen,lookup_linke_turbidity,perez,get_extra_radiation
from Sandia_model import SandiaModel
from One_diode_Batzelis import One_diode_Batzelis
from sandia_parameter_selection import sandia_parameter_selection
from ttictoc import tic,toc
import math
import pandas as pd
import numpy as np
import csv

## Input variables
# global_rad: global radiation (W/m^2)
# doy: day of year (hourly)
# TA: ambient air temperature (K)
# u: wind velocity (m/s)
# T_ground: ground surface temperature (K)
# TZ: time zone relative to UTC
# Lat: Latitude (in decimal degrees)
# Lon: Longitude (in decimal degrees)
# alpha: absorptivity of the PV cell below glazing (dependent on the PV cell properties; may not be constant)
# em_ground: emissivity of the ground surface
# em_ground: emissivity of the PV module
# dt: time step (s)

# Initial conditions needed:
# T_module: temperature of the PV module (assumed to be uniform)

# We have to consider different types of PV systems to determine the total irradiance
# 1 axis (vertical, horizontal (south/west or north/south orientation), slope)/ 2 axis tracking
# non-tracking


###############
#   Switches  #
###############


if PV_type == 'Polycrystalline':
    cell = A*Polycrystalline['Cell'][0]*Polycrystalline['Cell'][1]*Polycrystalline['Cell'][2]
    glass = A*Polycrystalline['Glass'][0]*Polycrystalline['Glass'][1]*Polycrystalline['Glass'][2]
    backing = A*Polycrystalline['Backing'][0]*Polycrystalline['Backing'][1]*Polycrystalline['Backing'][2]
    insulation = A*Polycrystalline['Insulation'][0]*Polycrystalline['Insulation'][1]*Polycrystalline['Insulation'][2]
elif PV_type == 'Monocrystalline':
    cell = A*Monocrystalline['Cell'][0]*Monocrystalline['Cell'][1]*Monocrystalline['Cell'][2]
    glass = A*Monocrystalline['Glass'][0]*Monocrystalline['Glass'][1]*Monocrystalline['Glass'][2]
    backing = A*Monocrystalline['Backing'][0]*Monocrystalline['Backing'][1]*Monocrystalline['Backing'][2]
    insulation = A*Monocrystalline['Insulation'][0]*Monocrystalline['Insulation'][1]*Monocrystalline['Insulation'][2]
elif PV_type == 'Thinfilm':
    cell = A*Thinfilm['Cell'][0]*Thinfilm['Cell'][1]*Thinfilm['Cell'][2]
    glass = A*Thinfilm['Glass'][0]*Thinfilm['Glass'][1]*Thinfilm['Glass'][2]
    backing = A*Thinfilm['Backing'][0]*Thinfilm['Backing'][1]*Thinfilm['Backing'][2]
    insulation = A*Thinfilm['Insulation'][0]*Thinfilm['Insulation'][1]*Thinfilm['Insulation'][2]
# Heat capacity of PV module
if Insulated == 1:
    Cmodule = sum([cell,backing,glass,insulation])
else:
    Cmodule = sum([cell,backing,glass])


###########################################
# Read and interpolate meteorological input to timestep
###########################################

data = pd.read_csv(input_path,infer_datetime_format=True)
#data = pd.read_csv('../inputs/Red_Rock_input.csv',infer_datetime_format=True)

data['Datetime'] = lookup(data['Datetime'])
# calculate day of year
data['DOY'] = data['Datetime'].dt.dayofyear
# LH: local hour (1-24)
data['hour'] = data['Datetime'].dt.hour

data = data.set_index(['Datetime'])

# # wind direction (°)
# wd = data['WD_CSAT_Avg'].tolist()
#
# # u and v vector components of wd for interpolation
# u_comp = []
# v_comp = []
# for i in range(0,len(wd)):
#     u_comp.append(math.sin(wd[i]*math.pi/180))
#     v_comp.append(math.cos(wd[i]*math.pi/180))
#
# data['u_comp'] = u_comp
# data['v_comp'] = v_comp

# number of days in input data
days = int(len(data)/(3600/inputdt*24))

# resample to hourly timestep for diffuse_rad function
data_hourly = data.resample('60T').asfreq()

global_rad_hourly = data_hourly['SW_down'].tolist()
global_rad_hourly = np.array(global_rad_hourly)
global_rad_hourly_MJ = global_rad_hourly*3600/1000000
global_rad_hourly_MJ = global_rad_hourly_MJ.tolist()

doy = data_hourly['DOY'].tolist()
hour = data_hourly['hour'].tolist()
hour = np.array(hour)
LH = hour.tolist()

# function that calculates diffuse radiation, solar hour angle (omega), solar altitude
# it needs hourly data as input and creates hourly output data and global radiation in MJ
omega,sinalpha, solar_alt, omega_sunrise, omega_sunset, kt, Kdif = solar_geo(Lat,Lon,doy,LH,TZ,days,global_rad_hourly_MJ)
extra_dni = get_extra_radiation(doy)
#omega,sinalpha,solar_alt,omega_sunrise,omega_sunset,cos_theta_zh,theta_zh,gamma_sh,kt = solar_geo(Lat,Lon,doy,TZ,LH,global_rad_hourly_MJ)
Kdif.append(0)
Kdif.append(0)

omega = pd.Series(omega)
data_hourly['omega'] = omega.values

sinalpha = pd.Series(sinalpha)
data_hourly['sinalpha'] = sinalpha.values

solar_alt = pd.Series(solar_alt)
data_hourly['solar_alt'] = solar_alt.values

omega_sunrise = pd.Series(omega_sunrise)
data_hourly['omega_sunrise'] = omega_sunrise.values

omega_sunset = pd.Series(omega_sunset)
data_hourly['omega_sunset'] = omega_sunset.values

kt = pd.Series(kt)
data_hourly['kt'] = kt.values

Kdif = pd.Series(Kdif)
data_hourly['Kdif'] = Kdif.values

extra_dni = pd.Series(extra_dni)
data_hourly['extra_dni'] = extra_dni.values

# now resample and interpolate to model timestep
dt_str = str(dt)
data_dt_1 = data.resample(''+dt_str+'S').asfreq()
data_dt_1 = data_dt_1.interpolate() # interpolated data to timestep dt

data_dt_2 = data_hourly.resample(''+dt_str+'S').asfreq()
data_dt_2 = data_dt_2.interpolate() # interpolated data to timestep dt

global_rad = data_dt_1['SW_down'].tolist()
Kdif = data_dt_2['Kdif'].tolist()

try:
    Ldown_meas = data_dt_1['LW_down'].tolist()
except:
    Ldown_meas = None
try:
    SW_dif = data_dt_1['SW_dif'].tolist()
except:
    SW_dif = None

TA = data_dt_1['TA'].tolist()
TA = np.array(TA)
TA = TA+273.15 # element-wise
TA = TA.tolist()
#T_ground = data_dt_1['T_surface_est_sun'].tolist()
if Tsurf_switch == 0:
    T_ground_sun = data_dt_1['Tground_sun'].tolist()
    T_ground_sun = np.array(T_ground_sun)
    T_ground_sun = T_ground_sun+273.15
    T_ground_sun = T_ground_sun.tolist()

    T_ground_shade = data_dt_1['Tground_shade'].tolist()
    T_ground_shade = np.array(T_ground_shade)
    T_ground_shade = T_ground_shade+273.15
    T_ground_shade = T_ground_shade.tolist()

u = data_dt_1['u'].tolist()   # wind speed at ~1.5 m
RH = data_dt_1['RH'].tolist() # relative humidity (%)
#Tu = data_dt_1['Tu'].tolist() # turbulence intensity (%)
LH = data_dt_1['hour'].tolist()
LH = data_dt_1.index.hour
minute = data_dt_1.index.minute
time = data_dt_1.index.date
LH=LH+minute/60
LH=LH.tolist()

try:
    P = list( data_dt_1['p'])  ## read in pressure data (mb)
except:
    P = None

data_dt_1['Lv']   = (2.501-0.00237*data_dt_1['TA'])*10**6
data_dt_1['es']   = 6.11 * np.exp((data_dt_1['Lv']/ Rv ) * ( 1./273.15 - 1./ (273.15 + data_dt_1['TA']) ) ) # saturation vapour pressure
data_dt_1['ea']   =  data_dt_1['es'] * (data_dt_1['RH']/100.) # vapour pressure

# u_comp = data_dt_1['u_comp'].tolist()
# v_comp = data_dt_1['v_comp'].tolist()

# # calculating wind direction back from vector components
# wd = []
# for i in range(0,len(data_dt_1)):
#     wd_intermediate = math.atan(abs(u_comp[i])/abs(v_comp[i]))*180/math.pi
#     if u_comp[i] >= 0 and v_comp[i] >= 0:
#         wd.append(wd_intermediate)
#     elif u_comp[i] >= 0 and v_comp[i] < 0:
#         wd.append(180 - wd_intermediate)
#     elif u_comp[i] < 0 and v_comp[i] >= 0:
#         wd.append(360 - wd_intermediate)
#     else:
#         wd.append(180 + wd_intermediate)

########################
# Initialize variables #
########################

# initialize PV module temperature
T_module = TA[1]

# initialize soil temperatures for force-restore
Ts_shd = TA[1]
Ts_sun = TA[1]
Tm_sun    = TA[1] +2.
Tm_shd = TA[1] +2.

#T_ground = (T_ground_sun[1]+T_ground_shade[1])/2.
T_ground = TA[1]

# initialize temperature profile for finite difference scheme
Tsoil = TA[1]
Tsun = [Tsoil]*nz
Tshade = [Tsoil]*nz

# initialize some things
delta_Ts_sun =0.
delta_Tm_sun = 0.
delta_Ts_shade = 0.
delta_Tm_shade = 0.
Rn_sun_ground = 0.
Rn_shade_ground = 0.
Qg_sun = 0.
Qg_shade = 0.
Rn_sun_ground_vec = [0]*3
Rn_shade_ground_vec = [0]*3

omega = data_dt_2['omega'].tolist()
sinalpha = data_dt_2['sinalpha'].tolist()
solar_alt = data_dt_2['solar_alt'].tolist()
omega_sunrise = data_dt_2['omega_sunrise'].tolist()
omega_sunset = data_dt_2['omega_sunset'].tolist()
#print(omega)
#print(len(omega))
doy = data_dt_2['DOY'].tolist()
kt = data_dt_2['kt'].tolist()
extra_dni = data_dt_2['extra_dni'].tolist()

# Define an output table for collecting model results
Output_table = pd.DataFrame(columns = ['qconv','qconv_max','qconv_min','QH_masson','TA','T_module','T_module_masson','Tground','P_out','P_out_masson','total_rad','global_rad','diffuse_rad','poa_sky_diffuse','direct_normal_rad','beta_PV','gamma_sh','gamma_h','solar_alt','cos_theta_zh','cos_theta_h',
'omega','delta','theta_zh','qlw','Ldown','Tgrnd_shade','Tgrnd_sun','Ts_shd','Ts_sun','Rn_shade_ground','Rn_sun_ground','L_PV_in','kt','Ldown_NARP','Qg_sun','Qg_shade','T4cm_sun','T6cm_sun','T10cm_sun',
'T4cm_shade','T6cm_shade','T10cm_shade','theta_h','shade_frc','P','TD','doy','GH','Kdif_model','ld','ldnh','tau_b','L_shd','qconv_EP','doy','PV_albedo','PVF_PV','SVF_PV','GVF_PV'])
Tsurf_output = pd.DataFrame(columns = ['T'])
fast_output = pd.DataFrame(columns = ['Ts_sun','Ts_shd','Tm_shd','Tm_sun', 'Tmodule' ])

steps = int(inputdt/dt) # 3600 seconds for the input data timestep; requires hourly input data

if Power_mod == 'Sandia':
    AA,NcellSer, Isc0, Imp0, Vmp0, aIsc, aImp, C0, C1, BVmp, DiodeFactor, C2, C3, a0, a1, a2, a3, a4, b0, b1, b2, b3, b4, b5, DT0, fd, a, b = sandia_parameter_selection()

### a bit of code to estimate the duration of the model run
length_empty =  int(np.floor(5/100*steps*len(data)))+1
elapsed_time_vec = np.empty([length_empty,1])
time_flag = 1
###

# dictionary to store results
dict_results = {}
dict_counter = 0

# start loop trough timesteps
for i in range(steps*0,steps*(len(data)-4)):

    if i/(steps*(len(data)))*100 < 5:
        tic()

    if global_rad[i] < 0:
        global_rad[i] = 0.

    if not Ldown_meas is None:
        Ldown = Ldown_meas[i]

    # We have to determine somehow wether we have clear or overcast skies
    # one possibility would be to look at the ratio of potential global radiation and
    # actual radiation (clearness index kt in diffuse_rad.py)
    # also needed by PVarray_surface_eb.py, maybe put in a separate file as a function

    try:
        if kt[i] > 0.5: # clearness index; this does not work at night...
            em_sky = 0.95  # clear sky emissivity
            T_sky = TA[i]-20
        else:
            em_sky = 1  # overcast sky emissivity
            T_sky = TA[i]
    except:
        em_sky = 0.95  # clear sky emissivity
        T_sky = TA[i]-20

    # for now I just go with a clear sky
    sky_condition =1
    if sky_condition ==1:
        em_sky = 0.95  # clear sky emissivity
        T_sky = TA[i]-20
    elif sky_condition ==0:
        em_sky = 1  # overcast sky emissivity
        T_sky = TA[i]

##################################################
# Longwave downward radiation modelled with NARP #
##################################################
    ea = data_dt_1['ea'][i]
    if Ldown_meas is None:
    #if NARP_switch == 1:
        Ldown = ld_mod(TA[i],RH[i],ea)

#################################################
# calculate solar altitude/ solar declination
# and diffuse radiation after Ridley et al. 2010
# in order to calculate total irradiance on a tilted
# surface after Stackhouse et al. 2016
#################################################

    delta = []
    # Calculate hourly radiation on tilted surface
    #print(i)

    #declination angle delta
    sin_param = math.sin((0.98565*(doy[i]-2))*math.pi/180)
    cos_param = math.cos((0.98565*(doy[i]+10)+1.914*sin_param)*math.pi/180)
    asin_param = (math.asin(0.39779*cos_param))/math.pi*180
    delta = -asin_param

    # fix for interpolated omega at night time
    if not i==0:
        if cos_theta_zh <0:
            if doy[i] >= 1 and doy[i] <= 100:
                EQT=-(9*math.sin((doy[i]-1)/0.5))-5
            elif doy[i] >= 101 and doy[i] <= 242:
                EQT=(5*math.sin((doy[i]-100)/0.395))-1
            else:
                EQT= (18.6*math.sin((doy[i]-242)/0.685))-2.5
            AST = LH[i] + TZ + Lon/15 + EQT/60
            omega[i]=(AST-12)*360/24

    #print(i)
    # if omega[i]<omega[i-1]:
    #      break
    # cosine of the zenit angle
    cos_theta_zh = math.cos(Lat*math.pi/180)*math.cos(delta*math.pi/180)*math.cos(omega[i]*math.pi/180)+math.sin(delta*math.pi/180)*math.sin(Lat*math.pi/180)
    # zenit angle (degrees)
    theta_zh = math.acos(cos_theta_zh)/math.pi*180
    # hourly solar azimuth angle
    gamma_sh = math.degrees(math.asin((math.sin(omega[i]*math.pi/180)*math.cos(delta*math.pi/180))/math.sin(theta_zh*math.pi/180)))


    ############################################
    # run Bird model clear sky radiation model #
    ############################################

    ## calculate dew point temperature (TD)
    TD   = (4880.357-29.66*math.log(data_dt_1['ea'][i]))/(19.48-math.log(data_dt_1['ea'][i]))
    ## calculate direct, diffuse radiation
    if P is not None:
        #DR1,DF1,GL1,DR1F,DF1F = CLRSKY(cos_theta_zh,P[i],theta_zh,TA[i],TD,math.cos(gamma_sh),doy[i],ground_albedo,0,global_rad[i])
        if clear_sky_type == 'Bird':
            DNI,Kdif_bird,GH,ldnh,ld,Kdif_model,T_rayleigh,T_ozone,T_gases,T_water,T_aerosol,Air_mass = bird_model(P[i],theta_zh,global_rad[i],doy[i])
        else:
            relative_airmass = get_relative_airmass(theta_zh)
            absolute_airmass = get_absolute_airmass(relative_airmass, P[i]*100)
            linke_turbidity = lookup_linke_turbidity(time[i],doy[i], Lat, Lon, '//KLIMA-NAS/home/Modelle/pv_model/inputs/LinkeTurbidities.h5',interp_turbidity=True)
            GH,Kdif_model = ineichen(theta_zh, absolute_airmass, linke_turbidity)
            #print(GH)
    if SW_dif is None:
        ### Ridley et al. 2010 used for diffuse radiation
        diffuse_rad_l = Kdif[i]

        # if global_rad[i] > 0:
        #     #Kdif=(global_rad[i]-DR1F+global_rad[i]*DF1/(global_rad[i]+1.e-9))/2.
        #     Kdif = (GL1-DR1F+GL1*DF1/(GL1+1.e-9))/2.
        #     Kdir=global_rad[i]-Kdif
        # else:
        #     Kdir = 0.
        #     Kdif = 0.
    else:
        diffuse_rad_l = SW_dif[i]

    if SW_down_switch == 2:
        global_rad[i] = GH
        diffuse_rad_l = Kdif_model
    Kdir = global_rad[i]-diffuse_rad_l

    ######################
    #   tracking types   #
    ######################
    if tracking_type == 1: # flat
        beta_PV = 0
        gamma_h = 0 #azimuth of PV module
    if tracking_type == 2: # tilted, fixed
        beta_PV = beta_tilt
        gamma_h = gamma_h_1
    if tracking_type ==3:
        # for tracking system with horizontal axis, which is north-south oriented
        if omega[i] < 0:
            gamma_h = -90
        else:
            gamma_h = 90

    #gamma_h = 0 #pointing towards the equator; when tracking around vertical axis it would be gamma_h=gamma_sh

        ## tilt angle (after Braun and Mitchell 1983)
        # this is for a north-south oriented horizontal axis (panels tilt east-west)

        beta_0 = math.atan(math.tan(theta_zh*math.pi/180)*math.cos((gamma_h-gamma_sh)*math.pi/180))/math.pi*180

        if beta_0 >= 0:
            sigma_beta = 0
        else:
            sigma_beta = 1

        beta_PV = beta_0 + sigma_beta*180

        if beta_PV > beta_max:
            beta_PV = beta_max
        if cos_theta_zh<0:
            beta_PV = beta_night

    if tracking_type == 4: # 1-axis sloped
        # cos of incidence angle: angle between a direct solar ray and the surface normal
        cos_theta_h_dash = math.cos(theta_zh*math.pi/180)*math.cos(beta_dash*math.pi/180)+math.sin(theta_zh*math.pi/180)*math.sin(beta_dash*math.pi/180)*math.cos((gamma_sh-gamma_sloped)*math.pi/180)
        gamma_zero = gamma_sloped + math.atan((math.sin(theta_zh*math.pi/180)*math.sin((gamma_sh-gamma_sloped)*math.pi/180))/(cos_theta_h_dash*math.sin(beta_dash*math.pi/180)))/math.pi*180

        if (gamma_zero-gamma_sloped)*(gamma_sh-gamma_sloped)>=0:
            sigma_gamma_one = 0
        else:
            sigma_gamma_one = 1
        if (gamma_sh-gamma_sloped)>=0:
            sigma_gamma_two = 1
        else:
            sigma_gamma_two = -1
        gamma_h = gamma_zero + sigma_gamma_one*sigma_gamma_two*180

        beta_zero_dash = math.atan(math.tan(beta_dash*math.pi/180)/math.cos((gamma_h-gamma_sloped)*math.pi/180))/math.pi*180

        if beta_zero_dash>=0.0:
            sigma_beta_dash = 0
        else:
            sigma_beta_dash  = 1
        beta_PV = beta_zero_dash + sigma_beta_dash*180

    if tracking_type == 5: # 1-axis vertical
        gamma_h = gamma_sh
        beta_PV = beta_tilt

    if tracking_type == 6: # two-axis
        if cos_theta_zh>0:
            beta_PV = theta_zh
            gamma_h = gamma_sh
        else:
            beta_PV = 30
            gamma_h = 0

    ### calculate shade fraction and shadow length etc
    shade_frc,U,S_length,S_length_EW,S2_EW,B_EW,S1_EW,L_shd,U_EW,cos_beta_shd,psi = shade_frc_fct(gamma_sh,beta_PV,solar_alt[i],cos_theta_zh)
    shade_frc = shade_frc_gen(gamma_h,gamma_sh,beta_PV,solar_alt[i])

    # cos of incidence angle: angle between a direct solar ray and the surface normal
    cos_theta_h = math.cos(theta_zh*math.pi/180)*math.cos(beta_PV*math.pi/180)+math.sin(theta_zh*math.pi/180)*math.sin(beta_PV*math.pi/180)*math.cos((gamma_sh-gamma_h)*math.pi/180)
    if cos_theta_h>1.0:
        cos_theta_h = 1.0

    # direct normal radiation
    if cos_theta_zh>0:
        direct_normal_rad = (global_rad[i]-diffuse_rad_l)/cos_theta_zh
        if direct_normal_rad<0.0:
            direct_normal_rad = 0.0
    else:
        direct_normal_rad = 0

    # model of incident angle refraction losses (reflectivity of glazing in dependence on incident angle)
    # Calculate PV reflectivity based on incidence angle after De Soto et al. (2006)
    theta_h_degree = math.acos(cos_theta_h)*180/math.pi
    # if cos_theta_h<1.0 and cos_theta_zh>0.01:
    if cos_theta_zh > 0.0:
        # for beam radiation
        theta_h_rad = math.acos(cos_theta_h)
        theta_r = math.asin(1/n*math.sin(theta_h_rad)) # angle of refraction (rad)
        if theta_r <0.00001:
            theta_r = 0.00001 # this is okay since tau_b is converging to a stable value asymptotically when theta_r -> 0
        tau_b = math.exp(-((K_glazing*Polycrystalline['Glass'][0])/math.cos(theta_r)))*(1-1/2*((math.sin(theta_r-theta_h_rad)**2)/(math.sin(theta_r+theta_h_rad)**2)+math.tan(theta_r-theta_h_rad)**2/(math.tan(theta_r+theta_h_rad)**2)))
        if tracking_type == 6:
            tau_b = 0.93
        tau_nil = math.exp(-K_glazing*Polycrystalline['Glass'][0])*(1-((1-n)/(1+n))**2) # transmittance normal to sun
        IAM_b = tau_b/tau_nil # IAM for beam radiation

        # for diffuse radiation from sky
        theta_h_dif_degree = 59.68 - 0.1388*beta_PV + 0.001497*beta_PV**2  # effective incidence angle, Duffie and Beckman 2013 p. 213
        theta_h_dif_rad = theta_h_dif_degree*math.pi/180
        theta_r = math.asin(1/n*math.sin(theta_h_dif_rad)) # angle of refraction
        tau_dif = math.exp(-((K_glazing*Polycrystalline['Glass'][0])/math.cos(theta_r)))*(1-1/2*((math.sin(theta_r-theta_h_dif_rad)**2)/(math.sin(theta_r+theta_h_dif_rad)**2)+math.tan(theta_r-theta_h_dif_rad)**2/(math.tan(theta_r+theta_h_dif_rad)**2)))
        tau_nil = math.exp(-K_glazing*Polycrystalline['Glass'][0])*(1-((1-n)/(1+n))**2) # transmittance normal to sun
        IAM_dif_sky = tau_dif/tau_nil

        # for diffuse radiation from ground
        theta_h_difg_degree = 90 - 0.5788*beta_PV + 0.002693*beta_PV**2 # effective incidence angle, Duffie and Beckman 2013 p. 213
        theta_h_difg_rad = theta_h_difg_degree*math.pi/180
        theta_r = math.asin(1/n*math.sin(theta_h_difg_rad)) # angle of refraction
        tau_difg = math.exp(-((K_glazing*Polycrystalline['Glass'][0])/math.cos(theta_r)))*(1-1/2*((math.sin(theta_r-theta_h_difg_rad)**2)/(math.sin(theta_r+theta_h_difg_rad)**2)+math.tan(theta_r-theta_h_difg_rad)**2/(math.tan(theta_r+theta_h_difg_rad)**2)))
        tau_nil = math.exp(-K_glazing*Polycrystalline['Glass'][0])*(1-((1-n)/(1+n))**2) # transmittance normal to sun
        IAM_difg = tau_difg/tau_nil
        # PV_albedo = PV_albedo_min + (1-IAM)

        # reflectivity based on Fresnel's law
        theta_r = math.asin(1/n*math.sin(theta_h_rad)) # angle of refraction (rad)
        if theta_r <0.00001:
            theta_r = 0.00001
        r_perp_unpol = ((math.sin(theta_h_rad-theta_r))**2)/((math.sin(theta_r+theta_h_rad))**2)
        r_par_unpol = ((math.tan(theta_h_rad-theta_r))**2)/((math.tan(theta_r+theta_h_rad))**2)
        PV_albedo = (r_perp_unpol + r_par_unpol)/2
        if tracking_type == 6:
            PV_albedo = PV_albedo_min
    else:
        IAM_b = 1
        IAM_dif_sky = 1
        IAM_difg = 1
        PV_albedo = PV_albedo_min

    #PV_albedo = 0.0028*theta_h_degree+PV_albedo_min # this is based on Protogeropoulos and Zachariou 2010
    theta_zh_degree = math.acos(cos_theta_zh)*180/math.pi
    # Air mass after King et al. 1998
    AM = 1/(cos_theta_zh+0.5057*(96.080-theta_zh_degree)**(-1.634))
    # Air mass modifier after DeSoto et al. 2006
    M = 0.935823*AM**0+0.054289*AM**1+-0.008677*AM**2+0.000527*AM**3+-0.000011*AM**4

    ## total radiation on a tilted surface (reflectivity considered) after Stackhouse 2016 p.26
    poa_sky_diffuse = 0
    if P is not None:
        if cos_theta_zh>0:
            if diffuse_rad_l>global_rad[i]:
                diffuse_rad_l = global_rad[i]-1.e-9
            # total_rad = (global_rad[i]-diffuse_rad_l)*(cos_theta_h/cos_theta_zh)+diffuse_rad_l*((1+math.cos(beta_PV*math.pi/180))/2)+global_rad[i]*ground_albedo*((1-math.cos(beta_PV*math.pi/180))/2)
            # after Duffie and Beckman 2013
            if diffuse_model == 'isotropic':
                total_rad_temp = (global_rad[i]-diffuse_rad_l)*(cos_theta_h/cos_theta_zh)*(1-PV_albedo)+diffuse_rad_l*((1+math.cos(beta_PV*math.pi/180))/2)*(1-PV_albedo)+global_rad[i]*ground_albedo*((1-math.cos(beta_PV*math.pi/180))/2)*(1-PV_albedo)
                # shortwave rad reaching cell surface after Duffie and Beckman 2013 p. 231
                total_rad = M*((global_rad[i]-diffuse_rad_l)*(cos_theta_h/cos_theta_zh)*tau_b*alpha+diffuse_rad_l*((1+math.cos(beta_PV*math.pi/180))/2)*tau_dif*alpha+global_rad[i]*ground_albedo*((1-math.cos(beta_PV*math.pi/180))/2)*tau_difg*alpha)

            elif diffuse_model == 'perez':
                    surface_azimuth = gamma_sh + 180
                    poa_sky_diffuse = perez(beta_PV, surface_azimuth, diffuse_rad_l, direct_normal_rad, extra_dni[i],
                              theta_zh, gamma_sh, AM,
                              model='allsitescomposite1990', return_components=False)
                    total_rad_temp = (global_rad[i]-diffuse_rad_l)*(cos_theta_h/cos_theta_zh)*(1-PV_albedo)+poa_sky_diffuse*(1-PV_albedo)+global_rad[i]*ground_albedo*((1-math.cos(beta_PV*math.pi/180))/2)*(1-PV_albedo)
                    total_rad = M*((global_rad[i]-diffuse_rad_l)*(cos_theta_h/cos_theta_zh)*tau_b*alpha+poa_sky_diffuse*tau_dif*alpha+global_rad[i]*ground_albedo*((1-math.cos(beta_PV*math.pi/180))/2)*tau_difg*alpha)

            total_rad_clear_theoretical = (GH-Kdif_model)*(cos_theta_h/cos_theta_zh)+Kdif_model*((1+math.cos(beta_PV*math.pi/180))/2)+GH*ground_albedo*((1-math.cos(beta_PV*math.pi/180))/2)
            if total_rad<0.0:
                total_rad=0.0
                total_rad_temp=0.0
            if total_rad > total_rad_clear_theoretical: # total radiation is not allowed to go above the theoretical total radiation for clear skies at the current time step, this is just a safety measure for sunrise/sunset
                total_rad = total_rad_clear_theoretical
                total_rad_temp = total_rad_clear_theoretical
                if total_rad<diffuse_rad_l*((1+math.cos(beta_PV*math.pi/180))/2):
                    total_rad=diffuse_rad_l*((1+math.cos(beta_PV*math.pi/180))/2)
                    total_rad_temp =diffuse_rad_l*((1+math.cos(beta_PV*math.pi/180))/2)

        else: # i.e. if the sun is below the horizon
            total_rad=diffuse_rad_l
            total_rad_temp=diffuse_rad_l
        #total_rad = total_rad*A
    else:
        if cos_theta_zh>0:
            if diffuse_rad_l>global_rad[i]:
                diffuse_rad_l = global_rad[i]-1.e-9
            # total_rad = (global_rad[i]-diffuse_rad_l)*(cos_theta_h/cos_theta_zh)+diffuse_rad_l*((1+math.cos(beta_PV*math.pi/180))/2)+global_rad[i]*ground_albedo*((1-math.cos(beta_PV*math.pi/180))/2)
            # after Duffie and Beckman 1991
            total_rad = M*((global_rad[i]-diffuse_rad_l)*(cos_theta_h/cos_theta_zh)*tau_b*alpha+diffuse_rad_l*((1+math.cos(beta_PV*math.pi/180))/2)*tau_dif*alpha+global_rad[i]*ground_albedo*((1-math.cos(beta_PV*math.pi/180))/2)*tau_difg*alpha)
            if total_rad<diffuse_rad_l:
                total_rad=diffuse_rad_l
        else: # i.e. if the sun is below the horizon
            total_rad=diffuse_rad_l
        #total_rad = total_rad*A

    ## total radiation after Klein 1977 (uncommenting makes model unstable...)
    # sunrise and sunset hour angles on tilted surface (in this implementation, only valid for tracking similar to RedRock site)
    # beta_cos = math.cos(beta_PV*math.pi/180)
    # beta_sin = math.sin(beta_PV*math.pi/180)
    # beta_tan = math.tan(beta_PV*math.pi/180)
    # delta_sin = math.sin(delta*math.pi/180)
    # delta_cos = math.cos(delta*math.pi/180)
    # delta_tan = math.tan(delta*math.pi/180)
    # Lat_sin = math.sin(Lat*math.pi/180)
    # Lat_cos = math.cos(Lat*math.pi/180)
    # gamma_h_cos = math.cos(gamma_h*math.pi/180)
    # gamma_h_sin = math.sin(gamma_h*math.pi/180)
    # gamma_h_tan = math.tan(gamma_h*math.pi/180)
    # A = Lat_cos/(gamma_h_sin*beta_tan)+Lat_sin/gamma_h_tan
    # B = delta_tan*(Lat_cos/gamma_h_tan-Lat_sin/(gamma_h_sin*beta_tan))

    # omega_sr = -min(omega_sunset[i],math.acos((A*B+math.sqrt(A**2-B**2+1))/(A**2+1))) # correct?
    # omega_ss = min(omega_sunset[i],math.acos((A*B+math.sqrt(A**2-B**2+1))/(A**2+1)))  # correct?
    # omega_ss_sin = math.sin(omega_ss*math.pi/180)
    # omega_ss_cos = math.cos(omega_ss*math.pi/180)
    # omega_sr_sin = math.sin(omega_sr*math.pi/180)
    # omega_sr_cos = math.cos(omega_sr*math.pi/180)
    # omega_sunset_sin = math.sin(omega_sunset[i]*math.pi/180)

    # Rb = ((beta_cos*delta_sin*Lat_sin)*(omega_ss-omega_sr)*(math.pi/180)-(delta_sin*Lat_cos*beta_sin*gamma_h_cos)*
            # (omega_ss-omega_sr)*(math.pi/180)+(Lat_cos*delta_cos*beta_cos)*(omega_ss_sin-omega_sr_sin)
            # +(delta_cos*gamma_h_cos*Lat_sin*beta_sin)*(omega_ss_sin-omega_sr_sin)-(delta_cos*beta_sin*gamma_h_sin)
            # *(omega_ss_cos-omega_sr_cos))/(2*(Lat_cos*delta_cos*omega_sunset_sin+(math.pi*180)*(omega_sunset[i]*Lat_sin*delta_sin)))
    # Beam_radiation = (global_rad[i]-diffuse_rad_l[i])*Rb

    # total_rad = Beam_radiation+diffuse_rad_l[i]*((1+math.cos(beta_PV*math.pi/180))/2)+global_rad[i]*PV_albedo*((1-
            # math.cos(beta_PV*math.pi/180))/2)

    #print('global rad: ')
    #print(global_rad[i])
    #print('diffuse_rad')
    #print(diffuse_rad_l[i])
    #print('total_rad: ')
    #print(total_rad)

    ###############################################################################
    ### calculate Tsurf using OHM and force-restore or finite difference scheme ###
    ###############################################################################

    # Calculate view factors for PV (assuming a row of PVs in front and behind), sky and ground

    VF_angle1 = math.atan(B_EW/2/(U_EW/2+width_rows))*180/math.pi
    VF_angle2 = math.atan(B_EW/2/(width_rows-U_EW/2))*180/math.pi
    VF_angle = VF_angle1 + VF_angle2

    width_rows_psi = math.sqrt(width_rows**2+(length_rows/2)**2)
    #VFangle_psi = math.atan((B/2)/width_rows_psi)*180/math.pi
    phi = math.tan((length_rows/2)/width_rows)  # angle (rad)
    #VF_angle_psi1 = math.atan((B/2)/(W_half+width_rows_psi))*180/math.pi
    VF_angle_psi1 = math.atan((B_EW/2)/(U_EW+width_rows_psi))*180/math.pi
    #VF_angle_psi2 = math.atan((B/2)/(width_rows_psi-W_half))*180/math.pi
    VF_angle_psi2 = math.atan((B_EW/2)/(width_rows_psi-U_EW))*180/math.pi
    VFangle_psi = VF_angle_psi1 + VF_angle_psi2

    VF_angle = (VF_angle+VFangle_psi)/2

    PVF_PV = (VF_angle)/180 # PV view fraction of the PV
    SVF_PV = (180-VF_angle)/180 # sky view fraction of the PV
    GVF_PV = (180-VF_angle)/180# ground view fraction of the PV

    # Calculate plan area fraction of PV needed later on
    PAF = U/width_rows

    if not i == 0:

        if Tsurf_switch == 1:
            Tmodule = fast_output['Tmodule'][i-1]
            Ts_shd = fast_output['Ts_shd'][i-1]
            Ts_sun = fast_output['Ts_sun'][i-1] ## becoming unstable in force-restore currently! JH

            Tm_sun_ =  fast_output['Tm_sun'][i-1]
            Tm_shd_ =  fast_output['Tm_shd'][i-1]

            # calculate net radiation for sun and shaded ground
            Rn_sun_ground =        (Ldown - (Ts_sun**4*em_ground*sigma) ) +  (global_rad[i] * (1.0 - ground_albedo))

            # at the moment the shaded ground has no shortwave input
            Rn_shade_ground    =  ((T_module**4*em_module_down*sigma) - (Ts_shd **4*em_ground*sigma) ) #+ ((global_rad[i]*0.7*0.3)*  (1.0 - ground_albedo)) + (diffuse_rad_l * 0.7)

            # calculate the ground heat flux for shaded and unshaded ground
            # OHM
            Qg_sun =   (a1*Rn_sun_ground)   + (a2*Rn_sun_ground)   + (a3)
            Qg_shade = (a1*Rn_shade_ground) + (a2*Rn_shade_ground) + (a3)

            # call force restore module
            delta_Ts_sun,     delta_Tm_sun = Ts_calc_surf(Qg_sun ,   Tm_sun_, Ts_sun)
            delta_Ts_shade, delta_Tm_shade = Ts_calc_surf(Qg_shade , Tm_shd_, Ts_shd)

	        # calculate surface and deep soil temperatures
            Ts_sun     = Ts_sun  + (dt * delta_Ts_sun)
            Tm_sun     = Tm_sun  + (dt * delta_Tm_sun)
            Ts_shd     = Ts_shd  + (dt * delta_Ts_shade)
            Tm_shd     = Tm_shd  + (dt * delta_Tm_shade)

        elif Tsurf_switch == 2:
            # calculate net radiation for sun and shaded ground
            # Tsun[nz-1] -> Rn_sun_ground -> Qg_sun -> Tsun ; this might converge in a loop(?)
            Rn_sun_ground_vec[0] =  Rn_sun_ground
            Rn_shade_ground_vec[0] =  Rn_shade_ground

            Rn_sun_ground =        (Ldown - (Tsun[nz-1]**4*em_ground*sigma) ) +  (global_rad[i] * (1.0 - ground_albedo))

            # note that shaded ground has some shortwave input (global rad reflected from sunlit ground to PV backside to shaded ground)
            Rn_shade_ground    =  (((T_module**4*em_module_down*sigma)+Tshade[nz-1]**4*em_ground*sigma*(1-em_module_down)) - (Tshade[nz-1]**4*em_ground*sigma) ) #+ ((global_rad[i]*0.3*ground_albedo)*  (1.0 - ground_albedo))

            Rn_sun_ground_vec[1] =  Rn_sun_ground
            Rn_shade_ground_vec[1] =  Rn_shade_ground

            # calculate polynomial
            x = [1,2]
            z = np.polyfit(x, Rn_sun_ground_vec[0:2], 1)
            f = np.poly1d(z)
            Rn_sun_ground_vec[2] = f(3) # linear estimation of Rn[t+1] to calculate dQstar/dt for 2nd term of OHM

            dt_Rn_sun = 0.5 * (Rn_sun_ground_vec[2]-Rn_sun_ground_vec[0])

            # calculate polynomial
            x = [1,2]
            z = np.polyfit(x, Rn_shade_ground_vec[0:2], 1)
            f = np.poly1d(z)
            Rn_shade_ground_vec[2] = f(3) # linear estimation of Rn[t+1]

            dt_Rn_shade = 0.5 * (Rn_shade_ground_vec[2]-Rn_shade_ground_vec[0])

            # calculate the ground heat flux for shaded and unshaded ground
            # OHM
            Qg_sun =   (a1*Rn_sun_ground)   + (a2*dt_Rn_sun)   + (a3)
            Qg_shade = (a1*Rn_shade_ground) + (a2*dt_Rn_shade) + (a3)

            # call finite difference scheme
            Tsun = FD_Tsurf(Tsun,Qg_sun)
            Tshade = FD_Tsurf(Tshade,Qg_shade)

	# uses modelled ground temperatures, when Tsurf_switch is set to 1 or 2
    if Tsurf_switch == 1:
        if global_rad[i] > 0.:
            T_ground = (shade_frc * Ts_shd) + ((1.-shade_frc) * Ts_sun)
        else:
            T_ground = (PAF * Ts_shd) + ((1-PAF) * Ts_sun) # weighted by plan area fraction
    elif Tsurf_switch == 2:
        if global_rad[i] > 0.:
            T_ground = (shade_frc * Tshade[nz-1]) + ((1.-shade_frc) * Tsun[nz-1])
            #T_ground = Tsun[nz-1]
        else:
            T_ground = (PAF * Tshade[nz-1]) + ((1-PAF) * Tsun[nz-1]) # weighted by plan area fraction
            #T_ground = Tsun[nz-1]
    else:
        if global_rad[i] > 0.:
            T_ground = (shade_frc * T_ground_shade[i]) + ((1-shade_frc) * T_ground_sun[i])
        else:
            T_ground = (PAF * T_ground_shade[i]) + ((1-PAF) * T_ground_sun[i])


    ## total radiation after Liu and Jordan model: this is only valid for surfaces tilted towards the south
    #psi = (beta_PV[i]+90)-solar_alt[i]
    #total_rad = (global_rad[i]-diffuse_rad_l[i])/sinalpha[i]*math.cos(psi*math.pi/180)+diffuse_rad_l[i]*((1+math.cos(beta_PV[i]*math.pi/180))/2)

    #print(diffuse_rad_l[i])

    ###############################
    # Longwave radiation transfer
    ###############################

    #qlw = A* sigma*(em_sky*T_sky**4+em_ground*T_ground[i]**4-em_module_down*T_module**4-em_module_up*T_module**4)
    #qlw = Ldown_meas[i]+A* sigma*(em_ground*T_ground[i]**4-em_module_down*T_module**4-em_module_up*T_module**4)

    # this is weighted now by the view fractions of the PV module, i.e. in a PV array each PV module partly "sees" the other modules in front and behind
    # now implemented in "2.5D", later on we can think of implementing this in 3D

    #VF_angle = math.atan((B/2)/width_rows)*180/math.pi # angle needed to calculate view fraction of the PV

    L_down = Ldown * SVF_PV  # SVF_PV = 0.5 when beta=0, so it needs to be multiplied by 2
    L_PV_in = (PVF_PV/2*sigma*em_module_up*T_module**4+PVF_PV/2*sigma*em_module_down*T_module**4)#*2
    L_ground = sigma*em_ground*T_ground**4*GVF_PV #* 2
    L_PV_up = sigma*em_module_up*T_module**4
    L_PV_down = sigma*em_module_down*T_module**4


    #qlw = L_down + L_PV_in + L_ground+(1-em_ground)*L_down - L_PV_up - L_PV_down
    qlw = L_down*em_module_up + L_PV_in + L_ground*em_module_down+(1-em_ground)*L_down*em_module_down - L_PV_up - L_PV_down

    #print('qlw: ')
    #print(qlw)

    #print('Tmodule: ')
    #print(T_module)
    #print('TD=', TD, '    TA=', TA)

    ###########################
    # Convective heat transfer
    ###########################

    # # ## Convective heat transfer TARP algorithm from EnergyPlus Engineering Reference p.45
    # Rf = 1.00
    # hf1 = 2.537*((2*L+2*W)*u[i]/(L*W))**(1/2)        # windward
    # hf2 = 2.537*1/2*((2*L+2*W)*u[i]/(L*W))**(1/2)    # lee ward
    # hf = hf1 + hf2
    # #
    # hn1 = 9.482*(abs(T_module-TA[i]))**(1/3)/(7.283-abs(math.cos(beta_PV*math.pi/180)))
    # hn2 = 1.810*(abs(T_module-TA[i]))**(1/3)/(1.382+abs(math.cos(beta_PV*math.pi/180)))
    # hn = hn1 + hn2
    # hc = hf + hn
    # qconv = hc * (T_module-TA[i])

    # free convection coefficient
    #hc_free = 1.31*(T_module-TA[i])**(1/3)           # free convection assumed from the lower/rear PV surface
    hc_free = 0
    #hc_forced = 2
    #hc_forced = 5.9 + 4.1*u[i]*((511+294)/(511+TA[i])) # referred to as Jurges formula in some studies
                                                 # e.g. Richards 2009, Heusinger and Weber 2015, Palyvos 2008
                                                 # forced convection assumed for the upper surface
    hc_forced =  4.6*math.exp(-0.6*u[i])+6.137*u[i]**0.78 # exact formula from Juerges 1924
    qconv_1 = 2*(hc_forced+hc_free)*(T_module-TA[i])

    L_char_kum = 0.802                         # characteristic length Kumar and Mullick 2010
    L_ratio = L_char**(-0.2)/L_char_kum**(-0.2)
    hc_Kum = 6.9 + 3.87*u[i]
    qconv_2 = 2*hc_Kum * (T_module-TA[i]) * L_ratio # QH after Kumar and Mullick 2010

    L_char_sharp = 1.193
    L_ratio = L_char**(-0.2)/L_char_sharp**(-0.2)
    hc_sharp = 3.3*u[i]+6.5
    qconv_3 = 2*hc_sharp * (T_module-TA[i]) * L_ratio # QH after Sharples and Charlesworth 1998

    L_char_test = 0.976
    L_ratio = L_char**(-0.2)/L_char_test**(-0.2)
    hc_test = 2.56 * u[i] + 8.55
    qconv_4 = 2*hc_test * (T_module-TA[i]) * L_ratio # QH after Test et al 1981

        ## Convective heat flux after Karava et al. 2011
        # v_kar= 14.55*10**(-6) # kinematic viscosity of air
        # k_kar = 0.02534      # thermal conducitivty of air @ 15°C @ sea level
        # Re = u[i]*L_char/v_kar
        # Pr = 0.7
        # L_char_kar = 3.45
        # L_ratio = L_char**(-0.2)/L_char_kar**(-0.2)
        # Nu = (-0.3928*Tu[i]**2+0.3672*Tu[i]+0.0257)*Re**0.77*Pr**(1/3)
        # h_kar = Nu*k_kar/L_char
        # qconv = h_kar * (T_module-TA[i]) * L_ratio

    qconv = (qconv_1 + qconv_2 + qconv_3 + qconv_4)/4          # Average sensible heat flux
    qconv_max = max(qconv_1,qconv_2,qconv_3,qconv_4)
    qconv_min = min(qconv_1,qconv_2,qconv_3,qconv_4)


    ##############################
    # Electrical power generation
    ##############################


    if Power_mod == 'Masson':
        try:
            ## After Jones and Underwood 2001
            #P_out = CFF*(total_rad*math.log(k1*total_rad))/T_module
            # After Masson et al. 2014
            #P_out = (total_rad-PV_albedo*total_rad)*Eff_PV*min(1,1-0.005*(T_module-298.15))
            P_out = total_rad*Eff_PV*min(1,1-0.005*(T_module-298.15))
        except:
            P_out = 0

    elif Power_mod == 'Sandia':
        theta_h = math.acos(cos_theta_h)/math.pi*180
        # Sandia model
        P_out = SandiaModel(total_rad-diffuse_rad_l,diffuse_rad_l+ground_albedo * diffuse_rad_l*(math.sin(beta_PV*math.pi/180)),u[i],TA[i]-273.15,theta_zh,T_module-273.15,theta_h,AA,NcellSer, Isc0, Imp0, Vmp0, aIsc, aImp, C0, C1, BVmp, DiodeFactor, C2, C3, a0, a1, a2, a3, a4, b0, b1, b2, b3, b4, b5, DT0, fd, a, b)
    elif Power_mod == 'Batzelis':
        P_out = One_diode_Batzelis(total_rad, T_module)
    elif Power_mod == 'Notton':
        if total_rad>0.0:
            Eff = Eff_PV*(1 - 0.00048*(T_module-25+275.15)+0.12*math.log(total_rad*dust_factor))
            P_out = Eff * total_rad
        else:
            P_out = 0.0

    # Module temperature based on Euler time stepping (needs small dt)
    T_change = (qlw+absorptivity*total_rad_temp-P_out-qconv)/Cmodule  # with absorption fraction calc after Duffie and Beckman 1991
    T_module = T_module + dt * T_change


    # estimate time duration for the model run
    if i/(steps*(len(data)))*100 < 5:
        elapsed_time = toc()
        elapsed_time_vec[i] = elapsed_time

    # soil temperature calculation
    # fast_output= fast_output.append({'Ts_shd': Ts_shd, 'Ts_sun':Ts_sun, 'Tm_shd': Ts_shd, 'Tm_sun': Tm_sun, 'Tmodule':T_module},ignore_index=True)

################################
# Comparison with Masson model #
################################
    # f_PV_masson = L/width_rows
    # T_module_masson = TA[i] + 0.021*global_rad[i]
    # P_out_masson = global_rad[i]*Eff_PV*min(1,1-0.005*(T_module_masson-298.15))
    # LW_up_masson = em_module_up*sigma*T_module_masson**4+(1-em_module_up)*L_down
    # LW_down_PV_masson = sigma*TA[i]**4
    # QH_masson = ((1-PV_albedo)*global_rad[i]+L_down-LW_up_masson+L_ground-LW_down_PV_masson)-P_out_masson
#####################
# Write model output
#####################

# write model results in table
    if math.fmod(i, outputdt/dt)==0:  # write for every 30 min
            dict_results[dict_counter] = {'qconv':qconv,'hc_forced':hc_forced,'TA':TA[i],'T_module':T_module,'P_out':P_out,'total_rad':total_rad_temp,'global_rad':global_rad[i],'diffuse_rad':diffuse_rad_l,'direct_normal_rad':direct_normal_rad,'beta_PV':beta_PV,'gamma_sh':gamma_sh,
            'gamma_h':gamma_h,'solar_alt':solar_alt[i],'cos_theta_zh':cos_theta_zh,'cos_theta_h':cos_theta_h,'omega':omega[i],'delta':delta,'theta_zh':theta_zh,'qlw':qlw,'Ldown':Ldown,
            'kt':kt[i],'theta_h':theta_h_degree,'shade_frc':shade_frc,'GVF_PV':GVF_PV,'SVF_PV':SVF_PV,'PV_albedo':PV_albedo,'T_ground':T_ground}
            dict_counter = dict_counter + 1

    if i/(steps*(len(data)))*100 >4.99 and i/(steps*(len(data)))*100 < 5.01 and time_flag == 1:
        elapsed_time_mean = np.nanmean(elapsed_time_vec)
        total_time = elapsed_time_mean * steps*(len(data)-4)
        print('Total time for model run estimated to be: ' + str(round(total_time/60,2)) + ' min')
        time_flag = 0

    if math.fmod(i/(steps*(len(data)))*100, 10)==0:
        print('Simulation status @ '+str(i/(steps*(len(data)))*100)+'%')
Output_table = pd.DataFrame.from_dict(dict_results,"index")
Output_table = Output_table.astype(str)
Output_table.to_csv(output_path)
#Tsurf_output = Tsurf_output.astype(str)
#Tsurf_output.to_csv('../outputs/Tsurf_output.csv')
print('Simulation finished')
