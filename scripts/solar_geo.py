
import math


def solar_geo(Lat,Lon,doy,LH,TZ,days,global_rad):

    diffuse_rad_l = []    # diffuse radiation  (W/m^2)
    omega = []          # solar hour angle
    beta_PV = []        # tilt angle of the PV cell
    d = []
    Idif = []    # diffuse radiation (MJ)
    H0 = []

# Lat: Latitude (decimal degrees)
# Lon: Longitude (decimal degrees)
# doy: day of year (integer)
# TZ: time zone relative to UTC
# LH: local hour (1-24)
# days: number of days of available data
# global_rad: global radiation (MJ, hourly)

    Lat = Lat*(-1)
    Lon = Lon*(-1)

    # Calculate apparent solar time
    EQT=[]          # equation of time
    AST=[]          # apparent solar time
    solar_alt=[]    # solar altitude (°)
    delta=[]
    sinalpha=[]
    for i in range(0,len(global_rad)):
        if doy[i] >= 1 and doy[i] <= 100:
            EQT.append(-(9*math.sin((doy[i]-1)/0.5))-5)
        elif doy[i] >= 101 and doy[i] <= 242:
            EQT.append((5*math.sin((doy[i]-100)/0.395))-1)
        else:
            EQT.append((18.6*math.sin((doy[i]-242)/0.685))-2.5)
        AST.append(LH[i] + TZ - Lon/15 + EQT[i]/60)

    for i in range(0,len(global_rad)):
        omega.append((AST[i]-12)*360/24)
        delta.append(-23.45 * math.sin(math.radians((doy[i]-80)/370*360)))
        sinalpha.append(math.cos(math.radians(Lat))*math.cos(math.radians(delta[i]))*math.cos(math.radians(omega[i]))
                    +math.sin(math.radians(Lat))*math.sin(math.radians(delta[i])))
        solar_alt.append(math.degrees(math.asin(sinalpha[i])))
        #print(solar_alt)

# Calculate extraterrestrial radiation H0 (MJ)

    I_SC = 1353
    for i in range(0,len(doy)):
        I0 = I_SC * (1+ 0.033* math.cos(360*doy[i]/370*math.pi/180))*3600/1000000
        H0.append(I0* math.sin(math.radians(solar_alt[i])))

    # Calculate omega_s (negative for sunrise, positive for sunset)
    cos_omega_s = []
    omega_sunrise = []
    omega_sunset = []

    for i in range(0,len(doy)):
        cos_omega_s.append(-math.tan(math.radians(Lat))*
                math.tan(math.radians(delta[i])))
        omega_sunrise.append(-math.degrees(math.acos(cos_omega_s[i])))
        omega_sunset.append(math.degrees(math.acos(cos_omega_s[i])))

    # Calculate clearness index
    kt = []

    for i in range(0,len(doy)):
        if global_rad[i]>0:
            kt.append(global_rad[i]/H0[i])
        else:
            kt.append(0)

    # Calculate Kt
    Kt = []
    for i in range(1,days+1):
        GlobSum = 0
        HSum = 0
        for j in range(0,24):
            if solar_alt[(i-1)*24+j] > 10 and global_rad[(i-1)*24+j]>=0:

                GlobSum = GlobSum + global_rad[(i-1)*24+j]
                HSum = HSum + H0[(i-1)*24+j]
        for j in range(0,24):
            Kt.append(GlobSum/HSum)



    # Calculate persistence
    Persistence = []

    for i in range(0,len(global_rad)-1):
        if omega_sunrise[i] < omega[i] and omega_sunrise[i] > omega[i-1] and kt[i] > 0:
            Persistence.append(kt[i])
        elif omega_sunset[i] > omega[i] and omega_sunset[i] < omega[i+1] and kt[i] > 0:
            Persistence.append(kt[i])
        elif kt[i-1] > 0 and kt[i+1] > 0 and kt[i] > 0:
            Persistence.append((kt[i-1]+kt[i+1])/2)
        else:
            Persistence.append(0)

    # Calculate diffuse radiation

    d = []
    for i in range(0,len(global_rad)-2):
        if Persistence[i] > 0:
            d.append(1/(1+math.exp(-5.3796+6.6316*kt[i]+0.006*AST[i]-0.0077*solar_alt[i]+1.75*Kt[i]+1.3066*Persistence[i])))
        else:
            d.append(0)
        Idif.append(d[i] * global_rad[i])
        diffuse_rad_l.append(Idif[i]/3600*1000000)
    return omega,sinalpha,solar_alt,omega_sunrise,omega_sunset,kt,diffuse_rad_l
