
import math

"""
Computes direct and diffuse components of direct radiation

Taken from TUF3D code (Scott Krayenhoff)

"""

class IntersectException(Exception):
    def __init__(self, msg):
        self.msg = msg
    def __str__(self):
        return self.msg

#INOT=1365.*(1.0001+0.034221*COS(THETA_INOT)+0.001280*SIN(THETA_INOT)+0.000719*COS(2.*THETA_INOT)+0.000077*SIN(2.*THETA_INOT))


def  CLRSKY(CZ,PRESS,ZEN,AIR,DEW,CA,JDAY,alb_sfc,cloudtype,Ktotfrc):

#CZ = solar zenith
#CZ = (SIN(LAT)*SIN(DEC))-(COS(LAT)*COS(DEC)*COS(HL))

# PRES =  pressure (mb)


#ZEN solar zenith
#ZEN=ACOS(CZ)

# AIR = air temp (K)

# DEW = dew point (K)

# INOT -

# Ktotfrc = incoming shortwave (Wm-2)

# CA =  cos(azimuth angle)
#CA = max(-1.,min(1.,(SIN(DEC)-SIN(LAT)*CZ)/(COS(LAT)*SIN(ZEN))))

        PI=3.141593


        THETA = float(JDAY-1.)*(2.*math.pi)/365.
        INOT=1365.*(1.0001+0.034221*math.cos(THETA)+0.001280*math.sin(THETA)+0.000719*math.cos(2.*THETA)+0.000077*math.sin(2.*THETA))



        DR1=0.
        DF1=0.
        GL1=0.
        DR1F = 0.
        DF1F = 0.
        abs_aero=0.


        TCL = 1.0
        ## this assumes clear sky we can fix this later

        # c this is the cloud transmissivity!!! (Haurwitz, 1948), in Atwater and
        # c Brown JAM March 1974 (all assumed at 100% cloud cover)
        #       if(cloudtype.eq.0) then
        # c clear: (also affects ALPHAB (below) and comment out Orgill and
        # c  Hollands (below)
        #        TCL=1.
        #       elseif(cloudtype.eq.1) then
        # c cirrus:
        #        TCL=0.8717-0.0179*(PRESS/(101.325*CZ))
        #       elseif(cloudtype.eq.2) then
        # c cirrostratus:
        #        TCL=0.9055-0.0638*(PRESS/(101.325*CZ))
        #       elseif(cloudtype.eq.3) then
        # c altocumulus:
        #        TCL=0.5456-0.0236*(PRESS/(101.325*CZ))
        #       elseif(cloudtype.eq.4) then
        # c altostratus:
        #        TCL=0.4130-0.0014*(PRESS/(101.325*CZ))
        #       elseif(cloudtype.eq.7) then
        # c cumulonimbus:
        #        TCL=0.2363+0.0145*(PRESS/(101.325*CZ))
        #       elseif(cloudtype.eq.5) then
        # c stratocumulus/cumulus:
        #        TCL=0.3658-0.0149*(PRESS/(101.325*CZ))
        #       elseif(cloudtype.eq.6) then
        # c thick stratus (Ns?):
        #        TCL=0.2684-0.0101*(PRESS/(101.325*CZ))
        #       else
        #        write(6,*)'cloudtype must be between 0 and 7, cloudtype = ',
        #      &            cloudtype
        #       endif

        ZEND=ZEN



        #CCC see Iqbal p. 175-195 for much of this parameterization
        #C      M repr. thickness of atm. through which solar beam travels(rel. mass)

        if ZEND >= 90.:
               return  DR1,DF1,GL1,DR1F,DF1F


        if ZEND < 90.0:
                MR=1./(CZ+(0.15*((93.885-ZEND)**(-1.253))))
                if MR < 1.:
                        raise IntersectException('problem with solar routine (MR>0), MR = '+str(MR))
        M=(PRESS/101.325)*MR
        #TR is the transmissivity due to Rayleigh scattering

        #SO THAT SMALL SUN ELEVATION ANGLES DON'T GIVE LARGE KDIR FOR VERTICAL
        #SURFACES DUE TO TR INCREASING ABOVE 1 FOR ZENITH ANGLES APPROACHING 90
        TR=math.exp(-0.0903*(M**0.84)*(1.+M-M**(1.0074)))
        AHAT=2.2572
        if TR > 1.:
                TR=0.99
        #     UW is a water vapour factor...DEW is dewpoint in Celsius
        UW=0.1*math.exp((0.05454*DEW)+AHAT)
        X2=UW*MR*((PRESS/101.325)**0.75)*((273.15/(273.15+AIR))**0.5)
        AW=(2.9*X2)/(((1.+141.5*X2)**0.635)+5.925*X2)

        #  visibility in km (70=clean,20=turbid, 6=very turbid)
        visib=70.
        TA=(0.97-1.265*visib**(-0.66))**(M**0.9)

        #  the following value is fraction scattered (vs absorbed)
        #  (0.9 for rural/agricultural,0.6 for cities)
        WO=0.70

        #  for multiple reflection of diffuse, assume rel. air mass 1.66
        TAprime=(0.97-1.265*visib**(-0.66))**((1.66)**(0.9))

        #  ratio of forward scattering
        BAA=-0.3012*CZ**2+0.7368*CZ+0.4877
        ALPHAB=max(0.0685+(1.-TCL),0.0685+(1.-BAA)*(1.-TAprime)*WO)

        #  ozone (in cm) from Table in Iqbal p.89
        #  first is for lat=40, second for lat=50
        ozone=1.595e-8*JDAY**3-0.9642e-5*JDAY**2+0.001458*JDAY+0.2754
        #      ozone=2.266e-8*jday**3-1.347e-5*jday**2+0.001958*jday+0.2957
        XO=10.*ozone*MR
        AO=((0.1082*XO)/((1.+13.86*XO)**0.805))+(0.00658*XO)/(1.+(10.36*XO)**3)+(0.002118*XO)/(1.+(0.0042*XO)+(0.0000323*XO*XO))
        TO=1.-AO

        #
        #     CALCULATE DIRECT, DIFFUSE, AND GLOBAL RADIATION
        #
        #  direct
        DR1=INOT*CZ*(TO*TR-AW)*TA*TCL
        #  contributions to diffuse
        DRR=INOT*CZ*TO*(TA/2.0)*(1.-TR)*TCL
        DA=INOT*CZ*WO*BAA*(1.-TA)*(TO*TR-AW)*TCL
        #  diffuse due to multiple reflection between the surface and the atmosphere
        DS=alb_sfc*ALPHAB*(DR1+DRR+DA)/(1.-alb_sfc*ALPHAB)
        #  total incident diffuse
        DF1=DRR+DA+DS
        #  aerosol absorption
        ABS_AERO=INOT*CZ*(TO*TR-AW)*(1.-TA)*(1.-WO)
        DR1_save=DR1

        if (DR1 < 0.0) or (DR1 > 1400.):
                DR1=0.
                ABS_AERO=0.

        if (DF1 < 0.0) or (DF1 > 1400.):
                DF1=0.
                ABS_AERO=0.

        GL1=DR1+DF1
        if cloudtype > 0.:
        #  Orgill and Hollands correlation taken from Iqbal p.269
                if INOT*CZ <= 0.:
                        return  DR1,DF1,GL1,DR1F,DF1F
                else:
                        I_I0=GL1/(INOT*CZ)

        #  This is a polynomial fit to Orgill and Hollands
                if I_I0 >= 0.85:
                        DF1=0.159873*GL1
                else:
                        DF1=GL1*(-27.59*I_I0**6+66.449*I_I0**5-48.232*I_I0**4+7.7246*I_I0**3+1.3433*I_I0**2-0.5357*I_I0+1.)

                DR1=GL1-DF1


        #  Orgill and Hollands changes DR1, which can cause problems later when we
        #  find the beam solar flux density (perpendicular to the incoming solar)
        #  from the direct solar flux density (perpendicular to a horizontal surface);
        #  Therefore, for small solar elevation angles we use DR1 as originally
        #  calculated above (before the Orgill and Hollands parameterization), and
        #  we interpolate between the two so that they match at solar elevation of 10 degrees
        if ZEND > 85.0:
                DR1=min(DR1_save,GL1)
                DF1=GL1-DR1
        elif (ZEND > 80.0) and (ZEND <= 85.):
                zendiff=85.-ZEND
                weight=1.-math.sqrt(1.-zendiff*zendiff/25.)
                DR1=min(DR1*weight+DR1_save*(1.-weight),GL1)
                DF1=GL1-DR1
        else:
         #     should be fine as is
            return  DR1,DF1,GL1,DR1F,DF1F


        #  Now Orgill and Hollands correlation for observed (forcing) radiation
        if Ktotfrc > 0.:
                if INOT*CZ <= 0.:
                        return   DR1,DF1,GL1,DR1F,DF1F
                else:
                        I_I0F=Ktotfrc/(INOT*CZ)
        #This is a polynomial fit to Orgill and Hollands
                if I_I0F >= 0.85:
                        DF1F=0.159873*Ktotfrc
                else:
                        DF1F=Ktotfrc*(-27.59*I_I0F**6+66.449*I_I0F**5-48.232*I_I0F**4+7.7246*I_I0F**3+1.3433*I_I0F**2-0.5357*I_I0F+1.)
                DR1F=Ktotfrc-DF1F
        return  DR1,DF1,GL1,DR1F,DF1F



        #
        #
        #
        # c------------------------------------------------------
        # C
        # C     SUBROUTINE CLRSKY DETERMINES THE SOLAR RADIATION RECEIVED AT THE
        # C     SURFACE UNDER CLEAR SKY CONDITIONS (clouds have been added)
        # C
        #
        #
        #
        #       SUBROUTINE CLRSKY(CZ,PRESS,ZEN,AIR,DEW,INOT,DR1,DF1,GL1,
        #      &           CA,JDAY,alb_sfc,cloudtype,abs_aero,Ktotfrc,DR1F)
        #
        #       implicit none
        #
        #       REAL PI,ZEND,TR,AHAT,UW,AW,TA,WO,BAA,ALPHAB,XO,TO,M
        #       REAL DRR,DA,DS,ZEN,PRESS,AIR,DEW,DR1,DF1,Ktotfrc,DR1F,DF1F,I_I0F
        #       REAL GL1,INOT,AO,CZ,X2,CA,DR1_save,zendiff,weight
        #       real alb_sfc,visib,TAprime,ozone,MR,I_I0,TCL,abs_aero
        #       INTEGER JDAY,cloudtype
        #
        #       PI=3.141593
        #
        #       DR1=0.
        #       DF1=0.
        #       GL1=0.
        #       abs_aero=0.
        #
        # c this is the cloud transmissivity!!! (Haurwitz, 1948), in Atwater and
        # c Brown JAM March 1974 (all assumed at 100% cloud cover)
        #       if(cloudtype.eq.0) then
        # c clear: (also affects ALPHAB (below) and comment out Orgill and
        # c  Hollands (below)
        #        TCL=1.
        #       elseif(cloudtype.eq.1) then
        # c cirrus:
        #        TCL=0.8717-0.0179*(PRESS/(101.325*CZ))
        #       elseif(cloudtype.eq.2) then
        # c cirrostratus:
        #        TCL=0.9055-0.0638*(PRESS/(101.325*CZ))
        #       elseif(cloudtype.eq.3) then
        # c altocumulus:
        #        TCL=0.5456-0.0236*(PRESS/(101.325*CZ))
        #       elseif(cloudtype.eq.4) then
        # c altostratus:
        #        TCL=0.4130-0.0014*(PRESS/(101.325*CZ))
        #       elseif(cloudtype.eq.7) then
        # c cumulonimbus:
        #        TCL=0.2363+0.0145*(PRESS/(101.325*CZ))
        #       elseif(cloudtype.eq.5) then
        # c stratocumulus/cumulus:
        #        TCL=0.3658-0.0149*(PRESS/(101.325*CZ))
        #       elseif(cloudtype.eq.6) then
        # c thick stratus (Ns?):
        #        TCL=0.2684-0.0101*(PRESS/(101.325*CZ))
        #       else
        #        write(6,*)'cloudtype must be between 0 and 7, cloudtype = ',
        #      &            cloudtype
        #       endif
        #
        #       ZEND=ZEN*180/PI
        # CCC see Iqbal p. 175-195 for much of this parameterization
        # C      M repr. thickness of atm. through which solar beam travels(rel. mass)
        #       if(zend.ge.90.0) goto 945
        #       MR=1./(CZ+(0.15*((93.885-ZEND)**(-1.253))))
        #       if(MR.lt.1.) then
        #        write(6,*)'problem with solar routine (MR>0), MR = ',MR
        #        stop
        #       endif
        #       M=(PRESS/101.325)*MR
        # C     TR is the transmissivity due to Rayleigh scattering
        #
        # c SO THAT SMALL SUN ELEVATION ANGLES DON'T GIVE LARGE KDIR FOR VERTICAL
        # c SURFACES DUE TO TR INCREASING ABOVE 1 FOR ZENITH ANGLES APPROACHING 90
        #        TR=EXP(-0.0903*(M**0.84)*(1.+M-M**(1.0074)))
        #        if(TR.gt.1.0) then
        #         write(6,*)'TR.gt.1,TR,M,MR,ZEND=',TR,M,MR,ZEND
        #         stop
        #       endif
        #
        #       AHAT=2.2572
        # C     UW is a water vapour factor...DEW is dewpoint in Celsius
        #       UW=0.1*EXP((0.05454*DEW)+AHAT)
        #       X2=UW*MR*((PRESS/101.325)**0.75)*((273.15/(273.15+AIR))
        #      $                                                       **0.5)
        #       AW=(2.9*X2)/(((1.+141.5*X2)**0.635)+5.925*X2)
        #
        # c  visibility in km (70=clean,20=turbid, 6=very turbid)
        #       visib=20.
        #       TA=(0.97-1.265*visib**(-0.66))**(M**0.9)
        #
        # c  the following value is fraction scattered (vs absorbed)
        # c  (0.9 for rural/agricultural,0.6 for cities)
        #       WO=0.70
        #
        # c  for multiple reflection of diffuse, assume rel. air mass 1.66
        #       TAprime=(0.97-1.265*visib**(-0.66))**((1.66)**(0.9))
        #
        # c  ratio of forward scattering
        #       BAA=-0.3012*CZ**2+0.7368*CZ+0.4877
        #       ALPHAB=max(0.0685+(1.-TCL),0.0685+(1.-BAA)*(1.-TAprime)*WO)
        #
        # c  ozone (in cm) from Table in Iqbal p.89
        # c  first is for lat=40, second for lat=50
        #       ozone=1.595e-8*jday**3-0.9642e-5*jday**2+0.001458*jday+0.2754
        # c      ozone=2.266e-8*jday**3-1.347e-5*jday**2+0.001958*jday+0.2957
        #       XO=10.*ozone*MR
        #       AO=((0.1082*XO)/((1.+13.86*XO)**0.805))+(0.00658*XO)/
        #      $(1.+(10.36*XO)**3)+(0.002118*XO)/(1.+(0.0042*XO)+
        #      $(0.0000323*XO*XO))
        #       TO=1.-AO
        #
        # C
        # C     CALCULATE DIRECT, DIFFUSE, AND GLOBAL RADIATION
        # C
        # c  direct
        #       DR1=INOT*CZ*(TO*TR-AW)*TA*TCL
        # c  contributions to diffuse
        #       DRR=INOT*CZ*TO*(TA/2.0)*(1.-TR)*TCL
        #       DA=INOT*CZ*WO*BAA*(1.-TA)*(TO*TR-AW)*TCL
        # c  diffuse due to multiple reflection between the surface and the atmosphere
        #       DS=alb_sfc*ALPHAB*(DR1+DRR+DA)/(1.-alb_sfc*ALPHAB)
        # c  total incident diffuse
        #       DF1=DRR+DA+DS
        # c  aerosol absorption
        #       ABS_AERO=INOT*CZ*(TO*TR-AW)*(1.-TA)*(1.-WO)
        #
        #       DR1_save=DR1
        #
        #       if(DR1.lt.0.0.or.DR1.gt.1400.)then
        #        DR1=0.
        #        ABS_AERO=0.
        #       endif
        #       if(DF1.lt.0.0.or.DF1.gt.1400.)then
        #        DF1=0.
        #        ABS_AERO=0.
        #       endif
        #
        #       GL1=DR1+DF1
        #       if(cloudtype.gt.0)then
        # c  Orgill and Hollands correlation taken from Iqbal p.269
        #        if(INOT*CZ.le.0.)then
        #         goto 47
        #        else
        #         I_I0=GL1/(INOT*CZ)
        #        endif
        # c  This is a polynomial fit to Orgill and Hollands
        #        if(I_I0.ge.0.85)then
        #         DF1=0.159873*GL1
        #        else
        #         DF1=GL1*(-27.59*I_I0**6+66.449*I_I0**5-48.232*I_I0**4
        #      &           +7.7246*I_I0**3+1.3433*I_I0**2-0.5357*I_I0+1.)
        #        endif
        #        DR1=GL1-DF1
        #       endif
        #
        # c  Orgill and Hollands changes DR1, which can cause problems later when we
        # c  find the beam solar flux density (perpendicular to the incoming solar)
        # c  from the direct solar flux density (perpendicular to a horizontal surface);
        # c  Therefore, for small solar elevation angles we use DR1 as originally
        # c  calculated above (before the Orgill and Hollands parameterization), and
        # c  we interpolate between the two so that they match at solar elevation of 10 degrees
        #       if(zend.gt.85.0) then
        #        DR1=min(DR1_save,GL1)
        #        DF1=GL1-DR1
        #       elseif(zend.gt.80.0.and.zend.le.85.) then
        #        zendiff=85.-zend
        #        weight=1.-sqrt(1.-zendiff*zendiff/25.)
        #        DR1=min(DR1*weight+DR1_save*(1.-weight),GL1)
        #        DF1=GL1-DR1
        #       else
        # c      should be fine as is
        #       endif
        #
        #
        # c  Now Orgill and Hollands correlation for observed (forcing) radiation
        #       if (Ktotfrc.gt.0.) then
        #        if(INOT*CZ.le.0.)then
        #         goto 47
        #        else
        #         I_I0F=Ktotfrc/(INOT*CZ)
        #        endif
        # c  This is a polynomial fit to Orgill and Hollands
        #        if(I_I0F.ge.0.85)then
        #         DF1F=0.159873*Ktotfrc
        #        else
        #         DF1F=Ktotfrc*(-27.59*I_I0F**6+66.449*I_I0F**5-48.232*I_I0F**4
        #      &           +7.7246*I_I0F**3+1.3433*I_I0F**2-0.5357*I_I0F+1.)
        #        endif
        #        DR1F=Ktotfrc-DF1F
        #       endif
        #
        #
        #  47   continue
        #  945  continue
        #       END
