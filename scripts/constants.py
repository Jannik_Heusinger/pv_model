# This file contains all constants needed for
# PV_model_MASTER.py, PVarray_surface_eb.py and NR01_longwave.py

dt=360            # time step (s)
inputdt = 1800          # time step of input data (s)
#inputdt = 3600
outputdt = 1800         # time step for output (s)
A = 1                   # surface area (m^2)
#rho_s = 0.2

### PV characteristics ###
absorptivity = 1       # overall absorptivity - this could be adapted for semi-transparent PV modules (not tested yet) - relevant for module temperature
alpha = 0.94           # absorptivity of the cell surface (below glazing) - relevant for power production


PV_albedo_min = 0.05             # reflectivity of the PV surface
n = 1.526               # refraction index of the cover glass
#n = 1.398
K_glazing = 4           # glazing extinction coefficient (m-1)
em_module_up = 0.82     # PV module upper surface emissivity
em_module_down = 0.97   # PV module lower surface emissivity
#em_module_down = 0.99   # PV module lower surface emissivity
h = 1.33            # PV height above ground (of horizontal axis, m)
L = 1.956           # length of PV module perpendicular to tilt axis (m)
W = 0.941            # width of PV module, necessary for calculating Pout and 2 axes shade fraction calc
# L=0.3
# W=0.2
L_char = 4*L*W/(2*L+2*W) # characteristic length for sens. heat flux calculations (m)

### tracking system related constants ###
beta_max = 60           # maximum tilt angle of PV (horixontal 1-axis tracking system)
beta_night = 30         # tilt angle of PV at night (horixontal 1-axis tracking system)
beta_tilt = 32          # tilt angle for non-tracking and vertical axis tracking
gamma_h_1 = 0         # azimuth angle of fixed, tilted system (0 facing equator, west 90, east -90)
beta_dash = 15          # tilt angle of sloped axis (sloped 1-axis tracking system)
gamma_sloped = 0        # azimuth angle of sloped axis (0 facing equator, west 90, east -90)


### Location and time zone ###
Lat = 32.55
Lon = -111.29
TZ = 7              # MST relative to UTC
#Lat = 41.9786 # KORD, Chicago
#Lon = -87.9048 # KORD, Chicago
#TZ = 6

### Universal constants ###
sigma = 5.669*10**(-8)  # Stefan-Boltzmann constant
Rv = 461.0              # Gas constant for water vapour [J K‐1 kg-1]

### PV power output ###
Eff_PV = 0.18          # conversion efficiency of the PV module
CFF = 1.22              # fill factor model constant (K m^2)
k1 = 10**6              # constant=K/I0 (m^2 K)

### PV array ###
width_rows = 5.64   # distance between rows (measured from one axis to the other, m)
#width_rows = 0.01
length_rows = 200   # length of PV panel rows in array (m)
h_NR01 = 6          # height of radiation sensor (m)

### Ground surface ###
ground_albedo = 0.3
em_ground = 0.95        # ground emissivity

####### LUMPS (OHM) parameters ########
# these values should be adjusted based on values given in Grimmond and Oke 1999
# if soil surface temperatures are not available
a1=0.32
a2=0.54
a3=-27.4

###### Force restore model parameters ######
C = 1350000.  # heat capacity of ground (J m^-3 K^-1)
K = 0.00000021 # thermal diffusivity of ground (m^2 s^-1)

###### Finite difference soil temp parameters #####
L_dom= 1 # length of model domain [m]
nz = 51     # number of gridpoints in z-direction

#k = 0.12 # heat conductivity (W m-1 K-1) desert sand
k = 0.3
#k = 1 # concrete
Cp = 921 # heat capacity (J kg-1 K-1)desert sand
#Cp = 1000 # concrete
ro = 1520 # density (kg m-3) desert sand
#ro = 2400 # concrete
#kappa_c = k/(ro*Cp) # thermal diffusivity (m2 s-1)
kappa_c = 0.5E-06 # thermal diffusivity assumption for loamy sand (m2 s-1)
dz = L_dom/(nz-1) # Spacing of gridpoints

ks = 0.12
Cps = 921
ros = 1520
kappa_s = ks/(ros*Cps)
