
%% Rotation implemented in UCRC-Solar

h = 1.33;
W = 1.96;
L = 0.99;
beta_PV = 47.8;       % angle between vertical and surface normal
gamma_h = 90;      % surface azimuth

% beta_dash_zero = atand(tand(beta_dash)/cosd(gamma_))

beta_dash = 15;
tracking_type = 2;

daytime = 'noon';  % 'morning', 'noon', 'evening'

if daytime == 'morning'
   gamma_h = -90;
   gamma_sh = -89;
   beta_PV = 47;
   solar_alt = 43;
elseif daytime == 'noon'
   gamma_h = 90;
   gamma_sh = 3;
   beta_PV = 0.55;
   solar_alt = 77.5;
else
   gamma_h = 90;
   gamma_sh = 89;
   beta_PV = 48;
   solar_alt = 42;
end

P1 = [L/2,-W/2,0];
P2 = [L/2,W/2,0];
P3 = [-L/2,W/2,0];
P4 = [-L/2,-W/2,0];

alpha = gamma_h;
beta = beta_PV;
gamma = 0;

cos_eps = cosd(beta_PV)/cosd(beta_dash);

if tracking_type == 2
   alpha = 0; 
   beta = 0;
   if gamma_h < 0
       gamma = beta_PV;
   else
       gamma = -beta_PV;
   end
end

if tracking_type == 3 % sloped tracking axis
        eps = acosd(cos_eps);
        alpha = 0;
        beta = beta_dash;
        if gamma_h < 0
            gamma = eps;
        else 
            gamma = -eps;
        end
end
           
alpha_rad = alpha * pi/180;
beta_rad = beta * pi/180;
gamma_rad = gamma * pi/180;

R_z = [cos(alpha_rad),-sin(alpha_rad),0;...
        sin(alpha_rad),cos(alpha_rad),0;...
        0,0,1];
R_y = [cos(beta_rad),0,sin(beta_rad);...
        0,1,0;...
        -sin(beta_rad),0,cos(beta_rad)];

R_x = [1,0,0;...
        0,cos(gamma_rad),-sin(gamma_rad);...
        0,sin(gamma_rad),cos(gamma_rad)];
    
point_matrix = [P1(1),P2(1),P3(1),P4(1);...
                P1(2),P2(2),P3(2),P4(2);...
                P1(3),P2(3),P3(3),P4(3)];
            
rotated_matrix1 = R_y*point_matrix;
rotated_matrix2 = R_z*rotated_matrix1;
rotated_matrix3 = R_x*rotated_matrix2;
    
rotated_matrix3(3,:) = rotated_matrix3(3,:) + h;

P11 = rotated_matrix3(:,1);
P21 = rotated_matrix3(:,2);
P31 = rotated_matrix3(:,3);
P41 = rotated_matrix3(:,4);

coor = [P11 , P21, P31, P41];
coor = coor';
figure()
quiver3(P11(1),P11(2),P11(3),s(1)*2,s(2)*2,s(3)*2,'LineWidth',2.0)
xlabel('x')
ylabel('y')
zlabel('z')
hold on
fill3(coor(:,1),coor(:,2),coor(:,3),'r', 'FaceAlpha', 0.5)
xlabel('x')
ylabel('y')
zlabel('z')


%% Shadow on ground
% x = positive --> south ; negative --> north
% y = positive --> west; negatvie --> east

% solar altitude: solar alt
% solar azimuth angle: gamma_sh
% solar_alt = 42;
% zenith_angle = 47;
% gamma_sh = 89;
% gamma_sh = -gamma_sh + 270;

cos_solar_alt = cos(solar_alt*pi/180);
sin_solar_alt = sin(solar_alt*pi/180);
cos_gamma_sh = cos(gamma_sh*pi/180);
sin_gamma_sh = sin(gamma_sh*pi/180);

% cos_zenith = cosd(47);
% sin_zenith = sind(47);

s = [cos_solar_alt * cos_gamma_sh, cos_solar_alt * sin_gamma_sh, sin_solar_alt];

% s_zenith = [sin_zenith * cos_gamma_sh, sin_zenith *sin_gamma_sh, cos_zenith]

% figure()
% quiver3(s(1),s(2),s(3),s(1)*2,s(2)*2,s(3)*2)
% xlabel('x')
% ylabel('y')
% zlabel('z')

p1 = [1,0,0];
p2 = [0,1,0];
p3 = [0,0,0];

b = p2-p1;      % direction vector of ground plane
c = p3-p1;      % direction vector of ground plane
n = cross(b,c); % normal vector of plane


dot_A = dot(n,P11'-p1);
dot_B = dot(n,s);
c_interm = dot_A/dot_B;
d_interm1 = c_interm * s;
ps1 = P11'-d_interm1;

dot_A = dot(n,P21'-p1);
dot_B = dot(n,s);
c_interm = dot_A/dot_B;
d_interm2 = c_interm * s;
ps2 = P21'-d_interm2;

dot_A = dot(n,P31'-p1);
dot_B = dot(n,s);
c_interm = dot_A/dot_B;
d_interm3 = c_interm * s;
ps3 = P31'-d_interm3;

dot_A = dot(n,P41'-p1);
dot_B = dot(n,s);
c_interm = dot_A/dot_B;
d_interm4 = c_interm * s;
ps4 = P41'-d_interm4;

coor = [P11 , P21, P31, P41] ;
coor = coor';
coor1 = [ps1;ps2;ps3;ps4]; 

shade_area = abs(((ps1(1)*ps2(2)-ps1(2)*ps2(1))+(ps2(1)*ps3(2)-ps2(2)*ps3(1))...
    +(ps3(1)*ps4(2)-ps3(2)*ps4(1))+(ps4(1)*ps1(2)-ps4(2)*ps1(1)))/2);


coor2 = [s; s*2];

%shade_frc = shade_area/ground_area

figure()
fill3(coor(:,1),coor(:,2),coor(:,3),'k', 'FaceAlpha', 1.0)
hold on
fill3(coor1(:,1),coor1(:,2),coor1(:,3),'k', 'FaceAlpha', 0.5)
hold on
quiver3(P11(1),P11(2),P11(3),-s(1)*0.95,-s(2)*0.95,-s(3)*0.95,'Color',[0.97,0.8,0.1],'LineWidth',2.0)
hold on
quiver3(P21(1),P21(2),P21(3),-s(1)*3.3,-s(2)*3.3,-s(3)*3.3,'Color',[0.97,0.8,0.1],'LineWidth',2.0)
hold on
quiver3(P31(1),P31(2),P31(3),-s(1)*3.3,-s(2)*3.3,-s(3)*3.3,'Color',[0.97,0.8,0.1],'LineWidth',2.0)
hold on
quiver3(P41(1),P41(2),P41(3),-s(1)*0.95,-s(2)*0.95,-s(3)*0.95,'Color',[0.97,0.8,0.1],'LineWidth',2.0)
ylim([-2.0 4])
xlim([-3 3])
zlim([0 3])
legend('Panel','Shaded ground','Direct rad. vector')
set(gca,'FontSize',12)
set(gca,'PlotBoxAspectRatio',...
    [1.66666666666667 1.66666666666667 1]);
xlabel('x (m)')
ylabel('y (m)')
zlabel('z (m)')
hold on
