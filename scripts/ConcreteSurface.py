## Script to model surface temperature of a concrete surface
import numpy as np
import pandas as pd
from FD_Tsurf_concrete import FD_Tsurf_concrete
from lookup_dates import lookup
from constants import *
import math

# Constants #
alpha = 0.1  # albedo
em = 0.90   # emissivity concrete
sigma = 5.669*10**(-8) # Stefan-Boltzmann constant
dt = 1800     # time step [s]

# Read data #
data = pd.read_csv('../inputs/PV_1month_val_data.csv',infer_datetime_format=True)
data['Datetime'] = lookup(data['Datetime'])
data = data.set_index(['Datetime'])

# now resample and interpolate to model timestep
dt_str = str(dt)
data_dt_1 = data.resample(''+dt_str+'S').asfreq()
data_dt_1 = data_dt_1.interpolate() # interpolated data to timestep dt

TA = data_dt_1['Tair_1.5m'].tolist()
TA = np.array(TA)
TA = TA+273.15 # element-wise
TA = TA.tolist()

SW_down = data_dt_1['Fsd_nr01_Avg'].tolist()
Ldown_meas = data_dt_1['Fld_nr01_Avg'].tolist()
u = data_dt_1['DS2_WS2_Avg'].tolist()   # wind speed at ~1.5 m

steps = int(1800/dt) # 1800 seconds for the input data timestep; requires half-hourly input data

T_surface = 20+273.15

# initialize temperature profile for finite difference scheme
T_ground = [T_surface]*nz
T_conv = [T_surface+1,T_surface] # for convergence test
Lup_vec = [0]*3                 # for curve fit to estimate Lup[t+1]
QH_vec = [0]*3                  # for curve fit to estimate QH[t+1]

# Define an output table for collecting model results
Output_table = pd.DataFrame(columns = ['QH','QG','T_surface','TA','RN','dT'])
Tsurf_output = pd.DataFrame(columns = ['T_ground'])
Converge_output = pd.DataFrame(columns = ['Lup','QH','QG','RN','T_surface','TA'])
# start loop trough timesteps
for i in range(steps*0,steps*48):#steps*2206):
    hc_free = 0
    hc_forced = 5.9 + 4.1*u[i]*((511+294)/(511+TA[i])) # referred to as Jurges formula in some studies
                                             # e.g. Richards 2009, Heusinger and Weber 2015, Palyvos 2008
                                             # forced convection assumed for the upper surface
    if i>2:
        Lup_vec[-3] = Lup_vec[-2]
        Lup_vec[-2] = Lup
        Lup = em*sigma*T_surface**4 # Lup[t]
        Lup_vec[-1] = Lup

        # calculate polynomial
        x = [1,2,3]
        z = np.polyfit(x, Lup_vec, 2)
        f = np.poly1d(z)
        Lup = f(4) # estimation of Lup[t+1]

        QH_vec[-3] = QH_vec[-2]
        QH_vec[-2] = QH
        QH = (hc_forced+hc_free)*A*(T_surface-TA[i])
        QH_vec[-1] = QH

        # calculate polynomial
        x = [1,2,3]
        z = np.polyfit(x, QH_vec, 2)
        f = np.poly1d(z)
        QH = f(4) # estimation of QH[t+1]

    for j in range(0,100000): # now loop for convergence with Lup[t+1] and QH[t+1] estimates
    #while abs(T_conv[-2]-T_conv[-1])>1.0E-4:
        Lup = em*sigma*T_surface**4


        QH = (hc_forced+hc_free)*A*(T_surface-TA[i])
        SW_up = SW_down[i]*alpha
        RN = SW_down[i]-SW_up+Ldown_meas[i]-Lup

        QG = RN-QH          # neglecting QE/Precipitation for now

        T_conv[-2] = T_surface
        T_ground = FD_Tsurf_concrete(T_ground,QG)
        T_surface=T_ground[-1]
        T_conv[-1] = T_surface
        if abs(T_conv[-2]-T_conv[-1])<1.0E-6:
            break
        #Converge_output = Converge_output.append({'T_surface':T_surface,'Lup':Lup,'QH':QG,'QG':QG,'RN':RN,'TA':TA[i]},ignore_index=True)
    #####################
    # Write model output
    #####################
    # write model results in table
    if math.fmod(i, 1800/dt)==0:  # write for every 30 min
        Output_table= Output_table.append({'QH':QH,'QG':QG,'T_surface':T_surface,'TA':TA[i],'RN':RN,'dT':T_ground[-1]-T_ground[-2]},ignore_index=True)
        Tsurf_output = Tsurf_output.append({'T_ground':T_ground},ignore_index=True)
        print(i)
Output_table = Output_table.astype(str)
Output_table.to_csv('../outputs/Concrete_output.csv')
Tsurf_output = Tsurf_output.astype(str)
Tsurf_output.to_csv('../outputs/T_Concrete_output.csv')

Converge_output  = Converge_output.astype(str)
Converge_output.to_csv('../outputs/Converge_output.csv')
