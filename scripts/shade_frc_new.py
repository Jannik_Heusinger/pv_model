## Script to calculate shade area of PV module on a plane

import math
import numpy as np
from constants import *
from switches import *

def shade_frc_gen(gamma_h,gamma_sh,beta_PV,solar_alt):

    ###############################################################################
    # Define the geometry and orienation of the PV module as a plane in 3D space  #
    ###############################################################################

    P1 = np.array([L/2,-W/2,0])
    P2 = np.array([L/2,W/2,0])
    P3 = np.array([-L/2,W/2,0])
    P4 = np.array([-L/2,-W/2,0])

    alpha = gamma_h # counterclockwise rotation about z-axis (gamma_h)
    beta = beta_PV # counterclockwise rotation about y-axis (beta_PV)
    gamma = 0 # counterclockwise rotation about x -axis ()

    if tracking_type == 3:
        alpha = 0
        beta = 0
        if gamma_h <= 0:
            gamma = beta_PV
        else:
            gamma = -beta_PV

    if tracking_type == 4:
        cos_eps = math.cos(beta_PV*math.pi/180)/math.cos(beta_dash*math.pi/180)
        eps = math.acos(cos_eps)*180/math.pi
        alpha = 0
        beta = beta_dash
        if gamma_h < 0:
            gamma = eps
        else:
            gamma = -eps

    alpha_rad = alpha*math.pi/180
    beta_rad = beta*math.pi/180
    gamma_rad = gamma*math.pi/180

    R_z = np.array([[math.cos(alpha_rad),-math.sin(alpha_rad),0],
            [math.sin(alpha_rad),math.cos(alpha_rad),0],
            [0,0,1]])

    R_y = np.array([[math.cos(beta_rad),0,math.sin(beta_rad)],
        [0,1,0],
        [-math.sin(beta_rad),0,math.cos(beta_rad)]])

    R_x = np.array([[1,0,0],
        [0,math.cos(gamma_rad),-math.sin(gamma_rad)],
        [0,math.sin(gamma_rad),math.cos(gamma_rad)]])

    point_matrix = np.array([[P1[0], P2[0], P3[0], P4[0]],
                            [P1[1], P2[1], P3[1], P4[1]],
                            [P1[2], P2[2], P3[2], P4[2]]])

    rotated_matrix = R_y.dot(point_matrix)
    rotated_matrix = R_z.dot(rotated_matrix)
    rotated_matrix = R_x.dot(rotated_matrix)

    rotated_matrix[2,:] =  rotated_matrix[2,:] + h;

    P11 = rotated_matrix[:,0]
    P21 = rotated_matrix[:,1]
    P31 = rotated_matrix[:,2]
    P41 = rotated_matrix[:,3]

    # import matplotlib.pyplot as plt
    # from mpl_toolkits.mplot3d import Axes3D
    # fig = plt.figure()
    # ax = fig.add_subplot(111, projection='3d')
    #
    # Axes3D.plot_surface(X, Y, Z)

    ##########################################
    #   Calculate shadow on ground surface  #
    #########################################
    # azimuth angle: gamma_sh

    # define points for ground surface plane
    p1 = np.array([1,0,0])
    p2 = np.array([0,1,0])
    p3 = np.array([0,0,0])


    cos_solar_alt = math.cos(solar_alt*math.pi/180)
    sin_solar_alt = math.sin(solar_alt*math.pi/180)
    cos_gamma_sh = math.cos(gamma_sh*math.pi/180)
    sin_gamma_sh = math.sin(gamma_sh*math.pi/180)

    # solar direction vector
    s = np.array([cos_solar_alt * cos_gamma_sh, cos_solar_alt * sin_gamma_sh, sin_solar_alt])

    b = p2-p1      # direction vector of ground plane
    c = p3-p1      # direction vector of ground plane
    n = np.cross(b,c); # normal vector of plane

    dot_A = np.dot(n,P11.T-p1)
    dot_B = np.dot(n,s)
    c_interm = dot_A/dot_B
    d_interm = c_interm * s
    ps1 = P11.T-d_interm;

    dot_A = np.dot(n,P21.T-p1)
    dot_B = np.dot(n,s)
    c_interm = dot_A/dot_B
    d_interm = c_interm * s
    ps2 = P21.T-d_interm;

    dot_A = np.dot(n,P31.T-p1)
    dot_B = np.dot(n,s)
    c_interm = dot_A/dot_B
    d_interm = c_interm * s
    ps3 = P31.T-d_interm;

    dot_A = np.dot(n,P41.T-p1)
    dot_B = np.dot(n,s)
    c_interm = dot_A/dot_B
    d_interm = c_interm * s
    ps4 = P41.T-d_interm;

    shade_area = abs(((ps1[0]*ps2[1]-ps1[1]*ps2[0])+(ps2[0]*ps3[1]-ps2[1]*ps3[0])+(ps3[0]*ps4[1]-ps3[1]*ps4[0])+(ps4[0]*ps1[1]-ps4[1]*ps1[0]))/2);

    #PV_area = abs(((P11(1)*P21(2)-P11(2)*P21(1))+(P21(1)*P31(2)-P21(2)*P31(1))+(P31(1)*P41(2)-P31(2)*P41(1))+(P41(1)*P11(2)-P41(2)*P11(1)))/2);
    if tracking_type < 4:
        ground_area = width_rows * W
        shade_frc = shade_area/ground_area
    else:
        ground_area = width_rows * width_rows
        shade_frc = shade_area/ground_area

    if shade_frc >1:
        shade_frc = 1

    return shade_frc
