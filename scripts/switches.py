# This file defines the model setup, input/output data file path and submodules to be used
from constants import *

# input data file path
# expexted headings in the input file:
# air temperature: TA
# relative humidity: RH
# wind speed: u
# atmospheric pressure: p
# diffuse radiation: SW_dif
# global horizontal radiation: SW_down
# longwave downward radiation from sky: LW_down
# ground surface temperature: T_ground
# Datetime: Datetime   Format: MM.DD.yyyy HH:MM
input_path = 'W:/Heusinger/Modelle/pv_model/Python/pv_model/inputs/Example_Input.csv'

#output data file path
output_path = 'W:/Heusinger/Modelle/pv_model/Python/pv_model/outputs/PV_output_newLongwave.csv'

# Measured ground surface temperatures(0), force-restore model (1) or finite difference scheme (2)
Tsurf_switch = 2

if Tsurf_switch ==2:
    # Compute stable timestep
    dt_stable = dz**2/kappa_c/4
    if dt > dt_stable:
        print('Warning: decrease time step dt - ground surface temperatures are unstable')

# Use global horizontal radiation input data (1) or simulate clear sky radiation (2)
SW_down_switch = 1

# Choose clear sky radiation model: 'Bird' or 'Ineichen'
clear_sky_type = 'Bird'

# Choose diffuse radiation model: 'isotropic' or 'perez'
diffuse_model = 'isotropic'

# Tracking type of the system: 1=Flat, 2=tilted,fixed, 3= 1-axis (horizontal axis), 4= 1-axis (sloped), 5= 1-axis (vertical), 6= 2-axis
tracking_type = 3

# Choose PV type: Monocrystalline or Polycrystalline
PV_type = 'Polycrystalline'
# PV module rear side insulated? 0= no insulation, 1= insulated
Insulated = 0

# Choose power model: 'Masson' or 'Sandia' or 'Batzelis' or 'Notton'
# Batzelis is an analytical solution to the One diode model (Batzelis 2017)
# Notton is the parameterization presented in Notton et al. 2005
# Sandia works only for a selection of specific PV modules which are available in the Sandia PV model database
Power_mod = 'Masson'

# PV module material characteristics
# List for all materials contains: thickness (d; m), density (rho; kg/m^3), specific heat capacity (C; J/(kg*K) )
# After Davis et al. 2003
Monocrystalline = {'Glass':[0.006,2500,840],'Cell':[0.00086,2330,712],
                    'Backing':[0.00017,1475,1130],'Insulation':[0.1016,55,1210]}
Polycrystalline = {'Glass':[0.006,2500,840],'Cell':[0.00038,2330,712],'Backing':
                    [0.00017,1475,1130],'Insulation':[0.1016,55,1210]}
Thinfilm = {'Glass':[0.000051,1750,1050],'Cell':[0.000001,2330,712],'Backing':
                     [0.000125,7900,477],'Insulation':[0.1016,55,1210]}
