## Analytical solution to the One-diode model by Batzelis and Papathanassiou
from mpmath import mp
import math
from constants import W,L

def One_diode_Batzelis(total_rad, T_module):
    # alpha_Isc: temperature coeffcieitn of short circuit current
    #beta_Voc: temperature coefficient of open circuit voltage
    alpha_Isc = 0.0005  # for Trina Solar TSM 14 PD module
    beta_Voc = -0.0032
    Voc0 = 45.6
    Isc0 = 9.00
    Imp0 = 8.51
    Vmp0 = 37.1
    T0 = 298.15
    total_rad = total_rad/1000
    if total_rad > 0.0:
        delta0 = (1-298.15*beta_Voc)/(50.1-298.15 * alpha_Isc)

        w0 = mp.lambertw(delta0)
        w0 = float(w0)

        a0 = delta0 * Voc0
        Rs0 = (a0*(w0-1)-Vmp0)/Imp0
        Rsh0 = a0*(w0-1)/(Isc0*(1-1/w0)-Imp0)
        Iph0 = (1 + Rs0/Rsh0)*Isc0
        Is0 = Iph0*math.exp(-1/delta0)

        deltaT = T_module - 298.15
        lambdaT = T_module/298.15

        Iph = Iph0 * total_rad *(1 + alpha_Isc*deltaT)
        Is = Is0*lambdaT**3*math.exp(47.1*(1-1/lambdaT))
        a = a0*lambdaT
        Rs = Rs0
        Rsh = Rsh0/total_rad

        w = w0 + 1/(1+delta0)*((beta_Voc-1/T0)/delta0 * deltaT/lambdaT + math.log(total_rad))

        Isc = total_rad * Isc0*(1 + alpha_Isc*deltaT)
        Voc = Voc0*(1 + delta0*lambdaT*math.log(total_rad) + beta_Voc*deltaT)

        epsilon0 = delta0/(1+ delta0) * (Voc0/Vmp0)
        epsilon1 = delta0 * (w0 - 1) * (Voc0/Vmp0) -1

        aImp = alpha_Isc + (beta_Voc-1/T0)/(w0-1)
        betaVmp = Voc0/Vmp0 * (beta_Voc/(1+delta0) + (delta0 * (w0 - 1) -1/(1 + delta0))/T0)

        Imp = total_rad * Imp0 * (1 + aImp*deltaT)
        Vmp = Vmp0 * (1 + epsilon0*lambdaT*math.log(total_rad)+epsilon1*(1-total_rad)+betaVmp*deltaT)

        Pmp = Vmp*Imp
        Pmp = Pmp/(L*W)
    else:
        Pmp = 0.0
    return Pmp
