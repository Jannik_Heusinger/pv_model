"""
this subrountine calculates:

solar elevation angle
zenith angle
hour angle
etc
"""
import math
from constants import Lat, Lon, TZ

omega = []          # solar hour angle

def solar_geo_array(Lat,Lon,doy,TZ,LH,global_rad):

# Lat: Latitude (decimal degrees)
# Lon: Longitude (decimal degrees)
# doy: day of year (integer)
# TZ: time zone relative to UTC
# LH: local hour (1-24)

    Lat = Lat*(-1)
    Lon = Lon*(-1)

    # Calculate apparent solar time
    if doy >= 1 and doy <= 100:
        EQT = -(9*math.sin((doy-1)/0.5))-5 # equation of time
    elif doy >= 101 and doy <= 242:
        EQT = (5*math.sin((doy-100)/0.395))-1
    else:
        EQT = (18.6*math.sin((doy-242)/0.685))-2.5
    AST = LH + TZ - Lon/15 + EQT/60 # apparent solar time

    omega = (AST-12)*360/24
    delta = -23.45 * math.sin(math.radians((doy-80)/370*360))
    sinalpha = math.cos(math.radians(Lat))*math.cos(math.radians(delta))*math.cos(math.radians(omega))+math.sin(math.radians(Lat))*math.sin(math.radians(delta))
    solar_alt = math.degrees(math.asin(sinalpha)) # solar altitude (°)


    # Calculate omega_s (negative for sunrise, positive for sunset)
    cos_omega_s = -math.tan(math.radians(Lat))*math.tan(math.radians(delta))
    omega_sunrise = -math.degrees(math.acos(cos_omega_s))
    omega_sunset = math.degrees(math.acos(cos_omega_s))

    sin_param = math.sin((0.98565*(doy-2))*math.pi/180)
    cos_param = math.cos((0.98565*(doy+10)+1.914*sin_param)*math.pi/180)
    asin_param = (math.asin(0.39779*cos_param))/math.pi*180
    delta = -asin_param; #declination angle

    # Calculate extraterrestrial radiation H0 (MJ)
    I_SC = 1353
    I0 = I_SC * (1+ 0.033* math.cos(360*doy/370*math.pi/180))*3600/1000000
    H0 = I0* math.sin(math.radians(solar_alt))

    # Calculate clearness index
    kt = []

    if global_rad>0:
        kt = global_rad/H0
    else:
        kt = 0

    # cosine of the zenit angle
    cos_theta_zh = math.cos(Lat*math.pi/180)*math.cos(delta*math.pi/180)*math.cos(omega*math.pi/180)+math.sin(delta*math.pi/180)*math.sin(Lat*math.pi/180)
    # zenit angle (degrees)
    theta_zh = math.acos(cos_theta_zh)/math.pi*180
    #  hourly solar azimuth angle
    gamma_sh = math.degrees(math.asin((math.sin(omega*math.pi/180)*math.cos(delta*math.pi/180))/math.sin(theta_zh*math.pi/180)))

    return omega,sinalpha,solar_alt,omega_sunrise,omega_sunset,cos_theta_zh,theta_zh,gamma_sh,kt
