"""
This module can model Ldown if Ldown data is unavailable
After Loridan et al. (2010)

see tech notes appendix

"""

def ld_mod(TA,RH,ea):

    import math
    TA_C = TA-273.15

    bcof = 0.015+1.9*10**(-4)*TA_C  # eq 7 Loridan et al., (2010)
    flcd = 0.185*math.exp(bcof*RH)        # eq 6 Loridan et al., (2010)
    es = 6.11 * 10**(7.5*TA_C/(237.3+TA_C))
    ea =0.611*math.exp(17.27*TA_C/(237.3+TA_C))/100*RH
    #ea = es/RH*100
    w = 46.5*(ea/TA)   # eq 6 Loridan et al., (2010)
    Emis_clr = 1-(1+w)*math.exp(-math.sqrt(1.2+(3*w)))
    #Emis_sky = Emis_clr+(1-Emis_clr)*(flcd^2)
    LD = (Emis_clr+(1-Emis_clr)*flcd)*((TA**4)*(5.67*(10**-8)))  ## eq 9 Loridan et al., (2010)

    return LD
